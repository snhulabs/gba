import 'styled-components';

// DefaultTheme redeclaration to enable theme code completion
declare module 'styled-components' {
  export interface DefaultTheme {
    colors: {
      white: string;
      primary: string;
      lightGray: string;
      action: string;
      gray: string;
      black: string;
      background: string;
      grayDark: string;
    };
    fonts: any;
  }
}
