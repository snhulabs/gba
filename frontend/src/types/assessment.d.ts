export declare type ComicDataType = {
  key: string;
  id: number;
  panels: Array<PanelType>;
};

export declare type PanelType = {
  key: string;
  panelImage: {
    url: string;
  };
  type: PANEL_TYPE;
  index: number;
  bubbles: Array<BubbleType>;
  span: number;
};

export declare type BubbleType = {
  text: string;
  x: number;
  y: number;
  width: number;
  type: string;
  orientation: string;
  pointer: PointerType;
};

export declare type Pointer = {
  orientation: string;
  thickness: number;
  direction: {
    x: number;
    y: number;
  };
};

export declare type Exam = {
  module: Array<Module>;
  randomModules: Array<Module>;
  panel2: {
    url: string;
  };
  panel3: {
    url: string;
  };
};

export declare type Module = {
  key: string;
  id: number;
  title: string;
  text: string;
  assessment: {
    id: number;
  };
};
