import styled from 'styled-components';

export default styled.div`
  font-size: 2.5rem;
  letter-spacing: 0.025em;
  position: relative;
`;
