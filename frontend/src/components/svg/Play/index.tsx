import React from 'react';
import Svg from './styled/Svg';

export default function Play() {
  return (
    <Svg viewBox="0 0 123 123" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="62" cy="62" r="59.5" stroke="#FDB913" strokeWidth="5" />
      <line
        x1="56.5"
        y1="43.5"
        x2="56.5"
        y2="78.5"
        stroke="#FDB913"
        strokeWidth="5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <line
        x1="74.4645"
        y1="61"
        x2="57"
        y2="43.5355"
        stroke="#FDB913"
        strokeWidth="5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <line
        x1="57"
        y1="78.4645"
        x2="74.4645"
        y2="61"
        stroke="#FDB913"
        strokeWidth="5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}
