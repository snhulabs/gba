import React from 'react';
import { motion } from 'framer-motion';

import SVG from './styled/Svg';

export default function Lines({
  className = '',
  isChecked = false,
  isCrossed = false,
}) {
  return (
    <SVG
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 375 401.67"
    >
      <g
        id="lines"
        opacity="0.05"
        fill="none"
        stroke="#0a3370"
        strokeLinecap="round"
        strokeWidth="10px"
      >
        <motion.path
          animate={{ pathLength: 1 }}
          transition={{ delay: 0.3 }}
          initial={{ pathLength: 0 }}
          d="M5,160.43v91.29"
        />
        <motion.path
          animate={{ pathLength: 1 }}
          transition={{ delay: 0.2 }}
          initial={{ pathLength: 0 }}
          d="M55.65,129V276.17"
        />
        <motion.path
          animate={{ pathLength: 1 }}
          transition={{ delay: 0 }}
          initial={{ pathLength: 0 }}
          d="M108,92.32v217"
        />
        <motion.path
          animate={{ pathLength: 1 }}
          transition={{ delay: 0.1 }}
          initial={{ pathLength: 0 }}
          d="M160.43,160.43v80.81"
        />
        <motion.path
          animate={{ pathLength: 1 }}
          transition={{ delay: 0.1 }}
          initial={{ pathLength: 0 }}
          d="M212.82,50.41V351.27"
        />
        <motion.path
          animate={{ pathLength: 1 }}
          transition={{ delay: 0.2 }}
          initial={{ pathLength: 0 }}
          d="M265.21,5V396.67"
        />
        <motion.path
          animate={{ pathLength: 1 }}
          transition={{ delay: 0.2 }}
          initial={{ pathLength: 0 }}
          d="M317.61,109.79v182.1"
        />
        <motion.path
          animate={{ pathLength: 1 }}
          transition={{ delay: 0.3 }}
          initial={{ pathLength: 0 }}
          d="M370,169.16v63.35"
        />
      </g>
      {isChecked && (
        <motion.path
          id="checked"
          animate={{ pathLength: 1 }}
          transition={{ delay: 0.3 }}
          initial={{ pathLength: 0 }}
          d="M63.16,224.48l84.55,84.66a14,14,0,0,0,21.69-2.33L310.16,87.45"
          fill="none"
          stroke="#0a3370"
          strokeLinecap="round"
          strokeWidth="10px"
          opacity="0.5"
        />
      )}
      {isCrossed && (
        <g
          id="crossed"
          fill="none"
          stroke="#0a3370"
          strokeLinecap="round"
          strokeWidth="10px"
          opacity="0.5"
        >
          <line x1="19.45" y1="42.78" x2="336.9" y2="360.22" />
          <line x1="354.55" y1="42.78" x2="37.1" y2="360.22" />
        </g>
      )}
    </SVG>
  );
}
