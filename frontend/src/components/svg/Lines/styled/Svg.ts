import styled from 'styled-components';

export default styled.svg`
  height: 100%;
  max-height: 665px;
  z-index: 0;
`;
