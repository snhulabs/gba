import React from 'react';

export default function Close() {
  return (
    <svg
      width="30"
      height="29"
      viewBox="0 0 30 29"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        opacity="0.1"
        d="M29 14.5C29 21.9242 22.7642 28 15 28C7.23579 28 1 21.9242 1 14.5C1 7.07584 7.23579 1 15 1C22.7642 1 29 7.07584 29 14.5Z"
        stroke="black"
        strokeWidth="2"
      />
      <rect
        opacity="0.1"
        x="10.4141"
        y="8"
        width="15"
        height="2"
        rx="1"
        transform="rotate(45 10.4141 8)"
        fill="black"
      />
      <rect
        opacity="0.1"
        x="21.0234"
        y="9.41406"
        width="15"
        height="2"
        rx="1"
        transform="rotate(135 21.0234 9.41406)"
        fill="black"
      />
    </svg>
  );
}
