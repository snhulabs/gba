import styled from 'styled-components';
import { motion } from 'framer-motion';

export default styled(motion.svg)`
  height: 100%;
  max-height: 665px;
  z-index: 0;
`;
