import React from 'react';
import Svg from './styled/Svg';

export default function Edit() {
  return (
    <Svg
      width="35"
      height="35"
      viewBox="0 0 35 35"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="17.5" cy="17.5" r="17.5" fill="#FDB913" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M22.8644 16.9218C23.2491 16.537 23.2491 15.9132 22.8644 15.5284L19.0326 11.6967C18.6479 11.3119 18.024 11.3119 17.6393 11.6967L10.6127 18.7233C10.4279 18.908 10.3241 19.1586 10.3241 19.42L10.3241 23.2517C10.3241 23.7958 10.7652 24.237 11.3093 24.237L15.1411 24.237C15.4024 24.237 15.653 24.1332 15.8378 23.9484L22.8644 16.9218Z"
        fill="white"
      />
      <rect
        x="20.4141"
        y="9"
        width="7.13918"
        height="2"
        rx="1"
        transform="rotate(45 20.4141 9)"
        fill="white"
      />
    </Svg>
  );
}
