import React from 'react';
import Svg from './styled/Svg';

export default function Pause() {
  return (
    <Svg viewBox="0 0 123 123" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="62" cy="62" r="59.5" stroke="#FDB913" strokeWidth="5" />
      <line
        x1="55.5"
        y1="44.5"
        x2="55.5"
        y2="79.5"
        stroke="#FDB913"
        strokeWidth="5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <line
        x1="69.5"
        y1="44.5"
        x2="69.5"
        y2="79.5"
        stroke="#FDB913"
        strokeWidth="5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}
