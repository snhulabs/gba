import React from 'react';
import Svg from './styled/Svg';

export default function Accept() {
  return (
    <Svg
      width="35"
      height="35"
      viewBox="0 0 35 35"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="17.5" cy="17.5" r="17.5" fill="#FDB913" />
      <rect
        x="12.4141"
        y="16"
        width="7.13918"
        height="2"
        rx="1"
        transform="rotate(45 12.4141 16)"
        fill="white"
      />
      <rect
        x="24.2539"
        y="14.623"
        width="11.0875"
        height="2"
        rx="1"
        transform="rotate(135 24.2539 14.623)"
        fill="white"
      />
    </Svg>
  );
}
