import React from 'react';

export default function Warning({ className = '' }) {
  return (
    <svg
      width="75"
      height="42"
      viewBox="0 0 75 42"
      className={className}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M36.1406 11.7852H39.8086V16.168L38.8594 24.2305H37.1133L36.1406 16.168V11.7852ZM36.2227 25.6133H39.7148V29H36.2227V25.6133Z"
        fill="#FDB913"
      />
      <circle cx="38" cy="21" r="19.5" stroke="#FDB913" strokeWidth="3" />
    </svg>
  );
}
