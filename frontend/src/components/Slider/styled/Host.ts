import styled from 'styled-components';

export default styled.div`
  width: 100%;
  z-index: 1;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
`;
