import React, { useContext } from 'react';

import LogoutButton from 'components/LogoutButton';

import UserFieldsWrapper from './styled/UserFieldsWrapper';
import Wrapper from './styled/Wrapper';
import UserId from './styled/UserId';
import { AuthContext } from 'context';

export default function Header() {
  const { user } = useContext(AuthContext);

  return (
    user && (
      <Wrapper>
        <UserFieldsWrapper>
          <UserId>User ID: {user.username} </UserId>
          <LogoutButton />
        </UserFieldsWrapper>
      </Wrapper>
    )
  );
}
