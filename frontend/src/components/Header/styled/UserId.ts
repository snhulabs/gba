import styled from 'styled-components';

export default styled.div`
  color: ${({ theme }) => theme.colors.grayDark};
  margin-right: 2rem;
`;
