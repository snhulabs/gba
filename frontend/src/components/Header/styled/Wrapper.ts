import styled from 'styled-components';

export default styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin: 0.5rem 0.5rem 0.5rem 0;

  > div {
    width: 100%;
  }
`;
