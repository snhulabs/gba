import React from 'react';

import Button from './styled/Button';
import Buffering from 'components/Buffering';

interface Props {
  children?: any;
  onClick: () => void;
  className?: string;
  disabled?: boolean;
  isLoading?: boolean;
}

export default function ActionButton({
  children,
  onClick = () => {},
  className = '',
  disabled = false,
  isLoading = false,
}: Props) {
  const handleClickEvent = () => {
    if (!disabled && !isLoading) onClick();
  };

  return (
    <Button
      onClick={handleClickEvent}
      className={className}
      disabled={disabled}
      isLoading={isLoading}
    >
      {isLoading ? <Buffering isLoading /> : children}
    </Button>
  );
}
