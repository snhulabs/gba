import styled, { css } from 'styled-components';

interface Props {
  disabled: boolean;
  isLoading?: boolean;
}

export default styled.div<Props>`
  display: flex;
  justify-content: center;
  width: 40%;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 7vh;
  cursor: pointer;
  position: relative;
  letter-spacing: 0.025em;
  font-family: Helvetica;
  font-size: 1.4vw;
  max-height: 70px;
  height: 10vh;
  align-items: center;

  background-color: ${({ theme }) => theme.colors.action};
  background-color: ${({ isLoading, theme }) => isLoading && theme.colors.gray};

  &:hover {
    text-decoration: underline;
  }

  ${({ disabled }) =>
    disabled &&
    css`
      background-color: ${({ theme }) => theme.colors.white};
    `}
`;
