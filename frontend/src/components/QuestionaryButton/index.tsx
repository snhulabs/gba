import React from 'react';

import Wrapper from './styled/Wrapper';

export default function index({ children, selected, onClick }) {
  return (
    <Wrapper selected={selected} onClick={onClick}>
      {children}
    </Wrapper>
  );
}
