import React, { useState } from 'react';

import Cover from './styled/Cover';

export default function CoverPanel({ onCoverClick }) {
  const [isHovered, setIsHovered] = useState(false);

  const hover = hovered => () => {
    setIsHovered(hovered);
  };

  return (
    <Cover
      onClick={onCoverClick}
      onMouseEnter={hover(true)}
      onMouseLeave={hover(false)}
      src={
        isHovered
          ? '/assets/images/panels/interactive_cover_hover.png'
          : '/assets/images/panels/interactive_cover.png'
      }
    />
  );
}
