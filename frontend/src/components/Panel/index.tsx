import React from 'react';
import { PanelType } from 'types/assessment';

import Bubble from 'components/Comics/Bubble';

import Wrapper from './styled/Wrapper';
import Image from './styled/Image';

interface Props {
  panel: PanelType;
  children?: any;
  key?: string;
}

export default function Panel({ panel, children }: Props) {
  return (
    <Wrapper>
      <Image src={panel.panelImage.url} />
      {panel.bubbles.map((bubble, idx) => (
        <Bubble key={`panel_${panel.key}_bubble_${idx}`} bubble={bubble} />
      ))}
      {children}
    </Wrapper>
  );
}
