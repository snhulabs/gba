import styled from 'styled-components';
import { rgba } from 'polished';
import { motion } from 'framer-motion';

export default styled(motion.div)`
  position: fixed;
  top: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background: ${({ theme }) => rgba(theme.colors.black, 0.4)};
  z-index: 10;
`;
