import React, { FunctionComponent } from 'react';

import Host from './styled/Host';
import Wrapper from './styled/Wrapper';

interface ModalProps {
  children: React.ReactNode | React.ReactNode[];
}

const Modal: FunctionComponent<ModalProps> = ({ children }) => {
  return (
    <Host
      animate={{ opacity: 1 }}
      initial={{ opacity: 0 }}
      transition={{ duration: 0.5 }}
      exit={{ opacity: 0 }}
    >
      <Wrapper
        animate={{ x: '-50%', y: '-50%' }}
        initial={{ x: '-53%', y: '-50%' }}
        exit={{ x: '-47%', y: '-50%' }}
        transition={{ duration: 0.5 }}
      >
        {children}
      </Wrapper>
    </Host>
  );
};

export default Modal;
