import styled from 'styled-components';
import Logo from 'components/Logo';
import gameLogo from '../../../public/assets/images/gameLogo.png';

const MenuLogo = styled(Logo)`
  margin: 0 auto;
  width: 6vw;
  min-height: 60px;
  max-height: 100px;
`;

const GameLogo = styled.div`
  margin: 20px auto;
  width: 6vw;
  max-width: 100px;
  min-height: 60px;
  max-height: 100px;
`;

const Image = styled.img`
  width: 100%;
`;

const LeftPanel = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 10rem;
  padding-top: 5vh;
  background-color: ${({ theme }) => theme.colors.background};
  flex-direction: column;
`;

import React from 'react';

export default function LogoPanel() {
  return (
    <LeftPanel>
      <MenuLogo />
      <GameLogo >
        <Image src={gameLogo} />
      </GameLogo>
    </LeftPanel>
  );
}
