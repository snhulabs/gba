import React, { useContext } from 'react';

import Wrapper from './styled/Wrapper';
import Image from './styled/Image';
import Points from './styled/Points';
import Counter from './styled/Counter';
import Glow from './styled/Glow';
import ProgressPanelPoint from 'components/ProgressPanelPoint';
import { AuthContext, AppContext } from 'context';

export default function ProgressPanel() {
  const { user } = useContext(AuthContext);
  const { currentModuleIdx } = useContext(AppContext);

  return (
    <Wrapper>
      <Image src="/assets/images/panels/progressPanel.png" />
      <Glow src="/assets/images/panels/hub_glow.png" />
      <Counter>{currentModuleIdx + 1}/12</Counter>
      <Points>
        {user &&
          user.modulesOrder.map(
            (module, idx) =>
              module && (
                <ProgressPanelPoint
                  key={module.title + idx}
                  isChecked={idx < currentModuleIdx}
                  isActive={idx === currentModuleIdx}
                  point={{ title: module.title }}
                />
              )
          )}
      </Points>
    </Wrapper>
  );
}
