import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: 30%;
  left: 20%;
  right: 16%;
  bottom: 5%;

  padding-right: 1rem;

  overflow: scroll;
  overflow-x: hidden;

  &::-webkit-scrollbar {
    width: 10px;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    background: rgba(255, 255, 255, 0.4);
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: #ffffff;
  }

  /* Handle on hover */
  &::-webkit-scrollbar-thumb:hover {
    background: #ffffff;
  }
`;
