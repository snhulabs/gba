import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: 13%;
  right: 10%;
  width: 26%;
  height: 13%;

  display: flex;
  align-items: center;
  justify-content: center;

  font-size: 1.5rem;
  color: #4faed5;
`;
