import styled from 'styled-components';

export default styled.div`
  display: flex;
  position: relative;

  min-height: 100vh;
  height: 100%;

  background-color: ${({ theme }) => theme.colors.white};
`;
