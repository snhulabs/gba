import React from 'react';

import LogoPanel from 'components/LogoPanel';

import Wrapper from './styled/Wrapper';
import PageContentWrapper from './styled/PageContentWrapper';
import Header from 'components/Header';

interface Props {
  children: any;
  className?: string;
}

export default function PageContent({ children, className }: Props) {
  return (
    <Wrapper>
      <LogoPanel />
      <PageContentWrapper className={className}>
        <Header />
        {children}
      </PageContentWrapper>
    </Wrapper>
  );
}
