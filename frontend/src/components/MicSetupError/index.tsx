import React, { useState } from 'react';
import { AnimatePresence } from 'framer-motion';

import WarningIcon from 'components/svg/Warning';

import Copy from './styled/Copy';
import Wrapper from './styled/Wrapper';
import MicErrorBackground from './styled/MicErrorBackground';
import InstructionWrapper from './styled/InstructionWrapper';
import Info from './styled/Info';
import Link from './styled/Link';
import Title from './styled/Title';
import BrowserImage from './styled/BrowserImage';
import ActionButton from 'components/ActionButton';
import Troubleshooting from 'components/Troubleshooting';

export default function MicSetupError() {
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
  const openModal = () => {
    setIsOpenModal(true);
  };
  // TODO Move to utils
  const reloadPage = () => {
    location.reload();
  };

  return (
    <Wrapper>
      <AnimatePresence exitBeforeEnter>
        {isOpenModal && <Troubleshooting setIsOpenModal={setIsOpenModal} />}
      </AnimatePresence>
      <MicErrorBackground isCrossed />
      <InstructionWrapper>
        <Title>An error occurred accessing the microphone</Title>
        <WarningIcon />
        <Copy>Your microphone wasn’t authorized.</Copy>
        <Copy>
          You might have clicked “Block” in the pop-up windo. To authorize your
          microphone, click the padlock in the address bar. In the pop-up
          window, select “Allow”, then refresh the page.
        </Copy>
        <BrowserImage src="/assets/images/micPermission.png" />
        <Copy>
          If you’re using Safari, refresh the page and click “Allow” in the
          pop-up window
        </Copy>
        <Info>
          <div>Still not working?</div>
          <Link target="_blank" onClick={openModal}>
            Click here to see some troubleshooting tips.
          </Link>
        </Info>
        <ActionButton onClick={reloadPage}>Try again</ActionButton>
      </InstructionWrapper>
    </Wrapper>
  );
}
