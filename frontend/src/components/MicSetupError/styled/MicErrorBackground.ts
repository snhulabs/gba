import styled from 'styled-components';
import Microphone from 'components/svg/Microphone';

export default styled(Microphone)`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -70%);

  width: 40vw;
  text-align: center;
`;
