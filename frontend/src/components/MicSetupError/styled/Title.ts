import styled from 'styled-components';

export default styled.div`
  font-weight: 400;
  font-size: 2vw;
  margin: 2vh 0;
  width: 40%;
`;
