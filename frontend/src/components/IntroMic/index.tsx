import React, { useRef, useState } from 'react';

import Wrapper from './styled/Wrapper';
import Title from './styled/Title';
import Description from './styled/Description';

export default function IntroMic({
  secondary = false,
}: {
  secondary?: boolean;
}) {
  return (
    <Wrapper>
      <Title>
        {secondary ? (
          'Let’s continue to test your mic.'
        ) : (
          <>
            Let’s test your mic to ensure we can
            <br />
            record your speaking task.
          </>
        )}
      </Title>
      <Description>
        {secondary ? (
          <>
            Your voice helps you come across as friendly and confident. <br />
            Record the phrase on screen as if you were talking to a potential employer.
          </>
        ) : (
          <>
            When meeting someone, the first few seconds are important. Be
            polite. Be friendly. <br />
            Repeat the phrase on screen as if you were meeting a potential
            employer.
          </>
        )}
      </Description>
    </Wrapper>
  );
}
