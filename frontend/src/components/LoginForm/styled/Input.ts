import styled from 'styled-components';

interface Props {
  colour: string;
}

export default styled.input<Props>`
  border: none;
  border-bottom: 1px solid black;
  border: 1px solid ${props => props.colour};
  outline: none;
  font-size: 1.6vw;
  margin: 1.3vw 0;
`;
