import styled, { css } from 'styled-components';

export default styled.div`
  ${({ theme }) => css`
    font-family: ${theme.fonts.helvetica};
  `}

  color: rgba(0,0,0,0.3);
`;
