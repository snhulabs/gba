import React, { useState } from 'react';

import Wrapper from './styled/Wrapper';
import Img from './styled/Img';

interface Props {
  className?: string;
  onClick: () => void;
}

export default function IncomingCallButton({ className, onClick }: Props) {
  const [isHovered, setIsHovered] = useState(false);

  const setHovered = state => () => {
    setIsHovered(state);
  };

  return (
    <Wrapper
      className={className}
      onMouseEnter={setHovered(true)}
      onMouseLeave={setHovered(false)}
      onClick={onClick}
    >
      {isHovered ? <Img src="/assets/images/panels/incomingCall_Hover.png" /> : <Img src="/assets/images/panels/incomingCall.png" />}
    </Wrapper>
  );
}
