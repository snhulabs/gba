import React, { useContext } from 'react';
import styled, { css } from 'styled-components';
import { AuthContext } from 'context';

const Button = styled.button`
  border-radius: 10px;
  border: 1px solid #3fa1cf;
  color: #3fa1cf;
  padding: 0 1rem;

  &:hover {
    background-color: #3fa1cf;
    color: #ffffff;
  }

  cursor: pointer;
`;

export default function LogoutButton() {
  const { logout } = useContext(AuthContext);

  const handleLogout = () => {
    logout();
  };

  return <Button onClick={handleLogout}>logout</Button>;
}
