import styled from 'styled-components';

export default styled.div`
  text-align: center;
  line-height: 1.7em;
  font-size: 1.3vw;
  margin-bottom: 3vh;
`;
