import React, { useEffect, useState } from 'react';

import PageContent from 'components/PageContent';
import Buffer from 'components/Buffering';

import manifest from 'manifest.json';
import { preloadImages } from 'utils/preload';

import Wrapper from './styled/Wrapper';

export default function Preloader() {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    preloadImages(
      manifest.map(file => file.path),
      () => {
        setIsLoading(false);
      }
    );
  }, []);

  return (
    isLoading && (
      <Wrapper>
        <PageContent>
          <Buffer isLoading />
        </PageContent>
      </Wrapper>
    )
  );
}
