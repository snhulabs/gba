import styled from 'styled-components';

export default styled.div`
  display: flex;
  margin-bottom: 0.5rem;
  color: white;

  svg {
    width: 1.4rem;
    height: 1.4rem;
    min-width: 1.4rem;
  }

  p {
    margin: 0;
    margin-left: 0.5rem;
    line-height: 1.4rem;
  }
`;
