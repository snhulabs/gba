import React from 'react';

import Wrapper from './styled/Wrapper';

interface Props {
  point: {
    title: string;
  };
  isChecked?: boolean;
  isActive?: boolean;
}

export default function ProgressPanelPoint({
  point,
  isChecked = false,
  isActive = false,
}: Props) {
  return (
    <Wrapper>
      <svg
        id="Layer_1"
        data-name="Layer 1"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 94 94"
      >
        {isChecked && (
          <g>
            <path
              d="M47.28,65.18h0a6.5,6.5,0,0,0,0-9.19L42.7,51.4,38.1,56c-.12.11-.23.22-.34.34a6.51,6.51,0,0,0-.92,7.59l1.26,1.25A6.48,6.48,0,0,0,47.28,65.18Z"
              fill="#fff"
            />
            <path
              d="M37.76,56.34c.11-.12.22-.23.34-.34l4.6-4.6L32.2,40.9A6.5,6.5,0,0,0,23,50.09L36.84,63.93A6.51,6.51,0,0,1,37.76,56.34Z"
              fill="#fff"
            />
            <path
              d="M70.39,32.9a6.51,6.51,0,0,0-9.19,0L42.7,51.4,47.29,56a6.5,6.5,0,0,1,0,9.19h0a6.48,6.48,0,0,1-9.18,0l-1.26-1.25a6.59,6.59,0,0,0,1.26,1.6,6.5,6.5,0,0,0,9.18-.34l23.11-23.1A6.51,6.51,0,0,0,70.39,32.9Z"
              fill="#fff"
            />
          </g>
        )}
        <circle
          cx="47"
          cy="47"
          r="42"
          fill={isActive ? 'rgba(255,255,255,0.4)' : 'none'}
          stroke="#fff"
          strokeWidth="10"
        />
      </svg>
      <p>{point.title}</p>
    </Wrapper>
  );
}
