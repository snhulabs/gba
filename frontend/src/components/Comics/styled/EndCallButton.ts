import styled, { keyframes } from 'styled-components';
import EndCallButton from 'components/EndCallButton';

const animateIn = keyframes`
  0% {
    transform: translateX(100%);
  }

  100% {
    transform: translateX(0);
  }
`;

export default styled(EndCallButton)`
  width: 20%;
  transform: translateX(100%);
  animation: ${animateIn} forwards ease-out 0.5s;
  animation-delay: 1s;
`;
