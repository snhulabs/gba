import styled from 'styled-components';

export default styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 25rem 25rem;
  grid-gap: 1vw;

  @media only screen and (max-width: 600px) {
    grid-template-columns: 60vw;
  }
`;
