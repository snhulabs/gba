import React, { useEffect, useRef, useState } from 'react';

import Wrapper from './styled/Wrapper';
import ActionButton from './styled/ActionButton';
import Audio from './styled/Audio';
import TryAgainButton from './styled/TryAgainButton';

interface Props {
  onTryAgain: () => void;
  onSubmit: () => void;
  audioSrc: string;
}

export default function AudioRecordingPanel({
  onSubmit,
  onTryAgain,
  audioSrc,
}) {
  const audioRef = useRef<HTMLAudioElement>();
  const [isUploading, setIsUploading] = useState(false);

  useEffect(() => {
    audioRef.current.load();
  }, [audioSrc]);

  const handleSubmitButton = async () => {
    setIsUploading(true);
    await onSubmit();
    setIsUploading(false);
  };

  return (
    <Wrapper>
      <Audio ref={audioRef} controls>
        <source src={audioSrc} />
      </Audio>
      {onTryAgain && (
        <TryAgainButton onClick={onTryAgain}>Try Again</TryAgainButton>
      )}
      <ActionButton onClick={handleSubmitButton} isLoading={isUploading}>
        Submit
      </ActionButton>
    </Wrapper>
  );
}
