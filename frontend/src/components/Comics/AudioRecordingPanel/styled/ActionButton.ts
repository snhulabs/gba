import styled from 'styled-components';
import ActionButton from 'components/ActionButton';

export default styled(ActionButton)`
  margin: 0.5rem 0;
  padding: 0.5rem 0;
  height: auto;
  font-size: 0.9rem;
`;
