import styled from 'styled-components';

export default styled.audio`
  height: 2rem;
  width: 80%;
  margin: -2rem 0 0.5rem 0;
`;
