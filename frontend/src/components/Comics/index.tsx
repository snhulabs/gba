import React, { useState, useEffect, useRef, useContext } from 'react';

import { ComicDataType, PanelType } from 'types/assessment';
import { PANEL_TYPE, ATTEMPTS_LIMIT } from 'utils/config';

import Panel from 'components/Panel';
import InteractivePanel from 'components/Comics/InteractivePanel';
import ResultPanel from 'components/Comics/ResultPanel';

import { useFlowManager, useMicrophoneRecording } from 'hooks';
import { STEPS } from 'hooks/useFlowManager';

import Wrapper from './styled/Wrapper';
import EndCallButton from './styled/EndCallButton';
import useEventManager from 'hooks/useEventManager';
import { sendAudio } from 'utils/api/recording';
import { AppContext } from 'context';
import GenericPanel from 'components/GenericPanel';

interface Props {
  data: ComicDataType;
  onRetry: (data) => void;
  onSubmit: (data) => void;
  onEndCall?: () => void;
  children?: any;
}

export default function Comics({
  data,
  onRetry,
  onSubmit,
  onEndCall,
  children,
}: Props) {
  const [isRecordingStarted, setIsRecordingStarted] = useState(false);
  const [
    isInteractivePanelUncovered,
    setIsInteractivePanelUncovered,
  ] = useState(false);
  const [hasRecordingFinished, setHasRecordingFinished] = useState(false);
  const [speechType, setSpeechType] = useState(false);
  const [activeInteractivePanel, setActiveInteractivePanel] = useState<number>(
    null
  );
  const {
    startRecording,
    stopRecording,
    audioSrc,
    upload,
    clearRecording,
  } = useMicrophoneRecording();
  const [attempt, setAttempt] = useState(1);

  useEffect(() => {
    setActiveInteractivePanel(getNextInteractivePanel());
  }, [data]);

  const getNextInteractivePanel = () => {
    return data.panels.findIndex(
      (panel, idx) =>
        activeInteractivePanel < idx && panel.type === PANEL_TYPE.interactive
    );
  };

  const onRecordingStarted = () => {
    setIsRecordingStarted(true);
    startRecording();
  };

  const onRecordingStopped = () => {
    setHasRecordingFinished(true);
    stopRecording();
  };

  const onSpeechTypeChange = speechType => {
    setSpeechType(speechType);
  };

  const getPanelOfType = (panel, idx) => {
    switch (panel.type) {
      case PANEL_TYPE.interactive:
        return (
          <InteractivePanel
            panel={panel}
            key={panel.key}
            onRecordingStarted={onRecordingStarted}
            isRecording={isRecordingStarted}
            onRecordingStopped={onRecordingStopped}
            hasRecordingFinished={hasRecordingFinished}
            isActive={data.panels[activeInteractivePanel] === panel}
            onSpeechTypeChange={onSpeechTypeChange}
          />
        );
      case PANEL_TYPE.resulting:
        return (
          <ResultPanel
            panel={panel}
            key={panel.key}
            isRecording={isRecordingStarted}
            hasRecordingFinished={hasRecordingFinished}
            onRecordingSubmitted={onRecordingSubmitted}
            onTryAgain={onTryAgain}
            canTryAgain={canTryAgain()}
            audioSrc={audioSrc}
            hasCompleted={idx < activeInteractivePanel}
          >
            {onEndCall && data.panels.length - 1 === idx && (
              <EndCallButton onClick={onEndCall} />
            )}
          </ResultPanel>
        );
      default:
        return <Panel panel={panel} key={panel.key} />;
    }
  };

  //TODO: refactor or document what is going on
  const isBeforeInteractiveAndResultPanel = panel => {
    let activePanelIdx = null;
    const resultPanelOffset = isInteractivePanelUncovered ? 1 : 0;
    for (let i = 0; i < data.panels.length; i++) {
      if (
        data.panels[i] === panel &&
        (activePanelIdx === null || i <= activePanelIdx + resultPanelOffset)
      )
        return true;

      if (
        data.panels[i].type === PANEL_TYPE.interactive &&
        data.panels[i] === data.panels[activeInteractivePanel]
      )
        activePanelIdx = i;
    }

    return false;
  };

  const isCovered = panel => {
    return (
      panel === data.panels[activeInteractivePanel] &&
      !isInteractivePanelUncovered
    );
  };

  const onCoverClick = () => {
    setIsInteractivePanelUncovered(true);
  };

  const renderPanel = (panel, idx) => (
    <GenericPanel
      key={panel.key + idx}
      span={panel.span || 1}
      isLocked={!isBeforeInteractiveAndResultPanel(panel)}
      isBehindCover={isCovered(panel)}
      onCoverClick={onCoverClick}
    >
      {getPanelOfType(panel, idx)}
    </GenericPanel>
  );

  const onRecordingSubmitted = async () => {
    // @ts-ignore
    const uploadedRecording = await upload(
      `${data.key}_panel${activeInteractivePanel + 1}`,
      attempt
    );
    await onSubmit({
      attempt,
      // @ts-ignore
      recordingId: uploadedRecording.id,
      name: `${data.key}_panel${activeInteractivePanel + 1}`,
      speechType: speechType,
    });

    // If there is no more interactive panels (comics completed)
    const nextActivePanel = getNextInteractivePanel();
    if (nextActivePanel > 0) {
      clearRecording();
      setActiveInteractivePanel(nextActivePanel);
      setHasRecordingFinished(false);
      setIsInteractivePanelUncovered(false);
      setAttempt(1);
      setIsRecordingStarted(false);
    }
  };

  const onTryAgain = () => {
    onRetry({ attempt });

    if (canTryAgain()) {
      setAttempt(attempt + 1);
      setIsRecordingStarted(false);
      setHasRecordingFinished(false);
    }
  };

  const canTryAgain = () => attempt <= ATTEMPTS_LIMIT;

  return (
    <Wrapper>
      {data.panels.map((panel, idx) => {
        return renderPanel(panel, idx);
      })}
      {children}
    </Wrapper>
  );
}
