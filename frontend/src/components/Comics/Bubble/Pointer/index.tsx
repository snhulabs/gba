import React, { useRef, useEffect, RefObject } from 'react';
import styled from 'styled-components';
import { BubbleType } from 'types/assessment';

const Wrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  z-index: 2;
  pointer-events: none;
`;

const Canvas = styled.canvas`
  width: 100%;
  height: 100%;
`;

interface Props {
  bubble: BubbleType;
  bubbleWrapper: RefObject<HTMLDivElement>;
}

export default function Pointer({ bubble, bubbleWrapper }: Props) {
  const wrapperRef = useRef<HTMLDivElement>();
  const canvasRef = useRef<HTMLCanvasElement>();
  const contextRef = useRef<CanvasRenderingContext2D>();

  useEffect(() => {
    canvasRef.current.width = wrapperRef.current.clientWidth;
    canvasRef.current.height = wrapperRef.current.clientHeight;
    contextRef.current = canvasRef.current.getContext('2d');

    drawPointer();
  }, []);

  const drawPointer = () => {
    if (!bubble.pointer) return null;

    const points = calculatePointersPoints();
    const ctx = contextRef.current;

    ctx.strokeStyle = 'black';
    ctx.fillStyle = 'white';
    ctx.lineWidth = 2;
    ctx.beginPath();
    ctx.moveTo(points[0][0], points[0][1]);

    for (let i = 1; i < points.length; i += 1) {
      const [x, y] = points[i];
      ctx.lineTo(x, y);
    }

    ctx.closePath();
    ctx.stroke();
    ctx.fill();

    ctx.clearRect(
      0,
      0,
      canvasRef.current.width,
      0.03 * canvasRef.current.width
    );
  };

  const calculatePointersPoints = () => {
    const { pointer } = bubble;
    const [bubblesCenterX, bubblesCenterY] = calcBubbleCenter();
    const thickness =
      pointer.thickness > 0
        ? pointer.thickness
        : isOrientationTopOrBottom()
        ? bubbleWrapper.current.clientWidth / 10 - 2
        : bubbleWrapper.current.clientHeight / 2 - 2;

    return [
      isOrientationTopOrBottom()
        ? [bubblesCenterX - thickness, bubblesCenterY]
        : [bubblesCenterX, bubblesCenterY - thickness],
      [
        pointer.direction.x * wrapperRef.current.clientWidth, //might be pointerRef
        pointer.direction.y * wrapperRef.current.clientHeight, //might be pointerRef
      ],
      isOrientationTopOrBottom()
        ? [bubblesCenterX + thickness, bubblesCenterY]
        : [bubblesCenterX, bubblesCenterY + thickness],
    ];
  };

  const calcBubbleCenter = () => {
    const bubblesCenterX =
      wrapperRef.current.clientWidth * bubble.x +
      wrapperRef.current.clientWidth * (bubble.width / 2);
    const bubblesCenterY =
      wrapperRef.current.clientHeight * bubble.y +
      bubbleWrapper.current.clientHeight / 2;

    return [bubblesCenterX, bubblesCenterY];
  };

  const isOrientationRightOrLeft = () =>
    bubble.pointer.orientation === 'left' ||
    bubble.pointer.orientation === 'right';
  const isOrientationTopOrBottom = () =>
    bubble.pointer.orientation === 'top' ||
    bubble.pointer.orientation === 'bottom';

  return (
    <Wrapper ref={wrapperRef}>
      <Canvas ref={canvasRef} />
    </Wrapper>
  );
}
