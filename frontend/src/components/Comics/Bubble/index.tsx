import React, { useRef, useEffect } from 'react';

import { BubbleType } from 'types/assessment';

import Wrapper from './styled/Wrapper';
import Background from './styled/Background';
import Border from './styled/Border';
import Text from './styled/Text';
import Pointer from 'components/Comics/Bubble/Pointer';

interface Props {
  bubble: BubbleType;
}

export default function Bubble({ bubble }: Props) {
  const bubbleWrapperRef = useRef<HTMLDivElement>();

  return (
    <>
      <Pointer bubble={bubble} bubbleWrapper={bubbleWrapperRef} />
      <Wrapper
        ref={bubbleWrapperRef}
        x={bubble.x * 100}
        y={bubble.y * 100}
        width={bubble.width * 100}
        orientation={bubble.orientation ? bubble.orientation : ''}
      >
        <Background />
        <Border />
        <Text>{bubble.text}</Text>
      </Wrapper>
    </>
  );
}
