import styled, { css } from 'styled-components';
import Background from './Background';
import Border from './Border';

interface Props {
  x: number;
  y: number;
  width: number;
  orientation: string;
}

const setBordersBasedOnOrientation = ({ orientation }) => {
  return css`
    ${Border} {
      ${orientation.includes('top') &&
      css`
        border-top: 0.3vw solid white;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      `}
      ${orientation.includes('left') &&
      css`
        border-left: 0.3vw solid white;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
      `}
      ${orientation.includes('bottom') &&
      css`
        border-bottom: 0.3vw solid white;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
      `}
      ${orientation.includes('right') &&
      css`
        border-right: 0.3vw solid white;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
      `}
    }
    ${Background} {
      ${orientation.includes('top') &&
      css`
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      `}
      ${orientation.includes('left') &&
      css`
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
      `}
      ${orientation.includes('bottom') &&
      css`
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
      `}
      ${orientation.includes('right') &&
      css`
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
      `}
    }
  `;
};

export default styled.div<Props>`
  position: absolute;
  padding: 2%;

  font-size: 0.8rem;
  line-height: 1.1em;
  text-align: center;
  text-transform: uppercase;

  ${({ x, y, width }) => css`
    top: ${y}%;
    left: ${x}%;
    width: ${width}%;
  `}

  ${setBordersBasedOnOrientation}

  @media only screen and (max-width: 600px) {
    font-size: 2.2vw;
  }
`;
