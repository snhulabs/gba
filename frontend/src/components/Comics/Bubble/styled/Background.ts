import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: 2px;
  right: 2px;
  bottom: 2px;
  left: 2px;
  border-radius: 5px;
  z-index: 3;

  background-color: white;
`;
