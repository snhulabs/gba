import React, { useState } from 'react';
import { PanelType } from 'types/assessment';

import Bubble from 'components/Comics/Bubble';
import AudioRecordingPanel from '../AudioRecordingPanel';

import Wrapper from './styled/Wrapper';
import Image from './styled/Image';
import InstructionsWrapper from './styled/InstructionsWrapper';
import Microphone from './styled/Microphone';
import { RECORDING_TIMEOUT, IS_DEBUG_PANELS } from 'utils/config';

interface Props {
  panel: PanelType;
  children?: any;
  isRecording: boolean;
  hasRecordingFinished: boolean;
  onRecordingSubmitted: () => void;
  onTryAgain: () => void;
  audioSrc: string;
  canTryAgain: boolean;
  hasCompleted: boolean;
}

export default function ResultPanel({
  panel,
  children,
  isRecording,
  hasRecordingFinished,
  onRecordingSubmitted,
  onTryAgain,
  audioSrc,
  canTryAgain,
  hasCompleted = false,
}: Props) {
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [render, setRender] = useState(false);

  const renderContent = () => {
    if (
      IS_DEBUG_PANELS ||
      (isSubmitted && hasRecordingFinished) ||
      hasCompleted
    ) {
      return renderFinalPanel();
    }

    if (hasRecordingFinished) {
      return renderAudioRecordingPanel();
    }

    if (isRecording) {
      return renderAudioRecordingInProgress();
    }

    return renderInstructions();
  };

  const handleRecordingSubmitted = async () => {
    await onRecordingSubmitted();
    setIsSubmitted(true);
  };

  const renderAudioRecordingPanel = () => {
    // if limit of attempts achieved, auto submit recording
    // if (!canTryAgain) {
    //   return handleRecordingSubmitted();
    // }

    return (
      <>
        <Microphone isOutlined={false} />
        <AudioRecordingPanel
          audioSrc={audioSrc}
          onSubmit={handleRecordingSubmitted}
          onTryAgain={canTryAgain && onTryAgain}
        />
      </>
    );
  };

  const renderInstructions = () => (
    <InstructionsWrapper>
      <p>
        DECIDE WHAT TYPE OF SPEECH TO USE AND RECORD YOUR AUDIO RESPONSE OF 1
        MINUTE OR LESS TO REVEAL THE FINAL PANEL.
      </p>
    </InstructionsWrapper>
  );

  const renderAudioRecordingInProgress = () => {
    return <Microphone isActive isCountdown time={RECORDING_TIMEOUT} />;
  };

  const onImageLoaded = () => {
    setRender(true);
  };

  const renderFinalPanel = () => (
    <>
      <Image src={panel.panelImage.url} onLoad={onImageLoaded} />
      {render &&
        panel.bubbles.map((bubble, idx) => (
          <Bubble key={`panel_${panel.key}_bubble_${idx}`} bubble={bubble} />
        ))}
      {children}
    </>
  );

  // @ts-ignore
  return <Wrapper>{renderContent()}</Wrapper>;
}
