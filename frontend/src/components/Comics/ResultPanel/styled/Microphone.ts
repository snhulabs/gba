import styled from 'styled-components';
import Microphone from 'components/svg/Microphone';

export default styled(Microphone)`
  width: 100%;
  height: 100%;
`;
