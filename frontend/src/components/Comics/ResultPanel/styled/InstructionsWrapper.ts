import styled from 'styled-components';

export default styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;

  background-color: white;
  color: ${({ theme }) => theme.colors.gray};
  text-align: center;
  border: 2px solid ${({ theme }) => theme.colors.gray};
  border-radius: 5px;

  p {
    width: 80%;
    font-size: 1.1rem;
  }
`;
