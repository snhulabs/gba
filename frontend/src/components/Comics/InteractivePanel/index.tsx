import React, { useState, useMemo, useRef, useEffect } from 'react';
import debounce from 'lodash/debounce';
import { PanelType } from 'types/assessment';

import Bubble from 'components/Comics/Bubble';

import { SPEECH_TYPES, RECORDING_TIMEOUT } from 'utils/config';

import GenericPanel from './styled/GenericPanel';
import Image from './styled/Image';
import Option from './styled/Option';
import Options from './styled/Options';
import Bullet from './styled/Bullet';
import Copy from './styled/Copy';
import Button from './styled/Button';
import ButtonContent from './styled/ButtonContent';
import Wrapper from './styled/Wrapper';

interface Props {
  panel: PanelType;
  onRecordingStarted: () => void;
  onRecordingStopped: () => void;
  onSpeechTypeChange: (speechType) => void;
  isRecording: boolean;
  isActive: boolean;
  hasRecordingFinished: boolean;
}

export default function InteractivePanel({
  panel,
  onRecordingStarted,
  onRecordingStopped,
  isRecording,
  hasRecordingFinished,
  isActive,
  onSpeechTypeChange,
}: Props) {
  const wrapperRef = useRef<HTMLDivElement>(null);
  const [activeType, setActiveType] = useState(null);
  const [isButtonHovered, setButtonHovered] = useState(false);
  // remove when finishing design impl
  const [render, setRender] = useState(false);
  const recordingTimeout = useRef(null);

  useEffect(() => {
    if (window.location.pathname === '/tutorial') setActiveType(1);

    window.addEventListener('resize', onResize);
    onResize();

    return () => {
      if (recordingTimeout.current) clearTimeout(recordingTimeout.current);
      window.removeEventListener('resize', onResize);
    };
  }, []);

  const onResize = debounce(() => {
    if (wrapperRef.current) {
      const options = document.getElementsByClassName('optionsCopies');
      for (let i = 0; i < options.length; i += 1) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        options[i].style.fontSize = `${
          0.05 * wrapperRef.current.clientWidth
        }px`;
      }
    }
  }, 500);

  const onSelectOption = optionIdx => () => {
    setActiveType(optionIdx);
    onSpeechTypeChange(SPEECH_TYPES[activeType]);
  };

  const setHovered = isHovered => () => {
    setButtonHovered(isHovered);
  };

  const renderOptions = () => {
    return (
      <Options>
        {SPEECH_TYPES.map((option, idx) => (
          <Option
            isActive={activeType === idx}
            onClick={onSelectOption(idx)}
            key={option}
          >
            <Bullet />
            <Copy className="optionsCopies">{option}</Copy>
          </Option>
        ))}
      </Options>
    );
  };

  const onRecordButtonClick = () => {
    if (activeType === null)
      return alert('Please, choose one from available options.');
    if (!isRecording) {
      onStartRecording();
    } else {
      onStopRecording();
    }
  };

  const onStopRecording = () => {
    onRecordingStopped();
    if (recordingTimeout.current) {
      clearTimeout(recordingTimeout.current);
    }
  };

  const onStartRecording = () => {
    onRecordingStarted();

    recordingTimeout.current = setTimeout(() => {
      onStopRecording();
    }, RECORDING_TIMEOUT);
  };

  const getButtonState = useMemo(() => {
    if (isButtonHovered) {
      return isRecording
        ? '/assets/images/panels/stopRecordingHovered.png'
        : '/assets/images/panels/recordAudioHovered.png';
    } else {
      return isRecording
        ? '/assets/images/panels/stopRecording.png'
        : '/assets/images/panels/recordAudio.png';
    }
  }, [isButtonHovered, isRecording]);

  const renderRecordButton = () => {
    return (
      <Button
        onClick={onRecordButtonClick}
        onMouseEnter={setHovered(true)}
        onMouseLeave={setHovered(false)}
      >
        <ButtonContent src={getButtonState} />
      </Button>
    );
  };

  const onImageLoaded = () => {
    setRender(true);
  };

  return (
    <Wrapper ref={wrapperRef} isEnabled={!hasRecordingFinished && isActive}>
      <Image
        src="/assets/images/panels/interactive.png"
        onLoad={onImageLoaded}
      />
      {renderOptions()}
      {renderRecordButton()}
      {render &&
        panel.bubbles.map((bubble, idx) => (
          <Bubble key={`panel_${panel.key}_bubble_${idx}`} bubble={bubble} />
        ))}
    </Wrapper>
  );
}
