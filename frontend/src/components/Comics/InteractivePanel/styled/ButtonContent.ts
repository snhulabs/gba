import styled from 'styled-components';

export default styled.img`
  width: 100%;
  height: 100%;
  max-width: 100%;
  max-height: 100%;
`;
