import styled, { css } from 'styled-components';
import GenericPanel from 'components/GenericPanel';

interface Props {
  isEnabled: boolean;
}

export default styled(GenericPanel)``;
