import styled, { css } from 'styled-components';

interface Props {
  isEnabled: boolean;
}

export default styled.div<Props>`
  width: 100%;
  height: 100%;

  ${({ isEnabled }) =>
    !isEnabled &&
    css`
      filter: grayscale(100%);
      pointer-events: none;
    `}
`;
