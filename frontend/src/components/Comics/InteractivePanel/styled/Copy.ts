import styled from 'styled-components';

export default styled.div`
  text-transform: uppercase;
  font-size: 1.3rem;
  transform: translate(9%, 1.5%);
`;
