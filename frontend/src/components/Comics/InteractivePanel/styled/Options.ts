import styled from 'styled-components';

export default styled.div`
  position: absolute;
  top: 26.85%;
  left: 26.7%;
  height: 26.2%;
  transform: skewX(-3deg);
  display: flex;
  flex-direction: column;
`;
