import styled from 'styled-components';

export default styled.div`
  border-radius: 50%;
  background-color: white;
  width: 0.8rem;
  height: 0.8rem;
`;
