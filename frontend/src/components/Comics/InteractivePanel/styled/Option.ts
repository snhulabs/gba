import styled, { css } from 'styled-components';
import Bullet from './Bullet';

interface Props {
  isActive: boolean;
}

export default styled.div<Props>`
  display: flex;
  align-items: center;

  color: rgba(255, 255, 255, 0.5);

  height: 33.33%;

  font-family: ${({ theme }) => {
    return theme.fonts.montserratBold;
  }};

  ${Bullet} {
    visibility: hidden;
  }

  ${({ isActive }) =>
    isActive &&
    css`
      color: rgba(255, 255, 255, 1);
      ${Bullet} {
        visibility: visible;
      }
    `}

  &:nth-child(2) {
    ${Bullet} {
      transform: translate(-3%, 5%);
    }
  }
  &:nth-child(3) {
    ${Bullet} {
      transform: translate(-3%, -4%);
    }
  }

  cursor: pointer;

  &:hover {
    color: rgba(255, 255, 255, 1);

    ${Bullet} {
      visibility: visible;
    }
  }
`;
