import React from 'react';

import Wrapper from './styled/Wrapper';
import CoverPanel from 'components/CoverPanel';
import Lock from './styled/Cover';
import { IS_DEBUG_PANELS } from 'utils/config';

interface Props {
  isLocked?: boolean;
  isBehindCover?: boolean;
  isEnabled?: boolean;
  children?: any;
  className?: string;
  span?: number;
  onCoverClick?: () => void;
}

export default function GenericPanel({
  isLocked,
  isBehindCover,
  children,
  className,
  span,
  onCoverClick,
}: Props) {
  return (
    <Wrapper span={span || 1} className={className}>
      {!IS_DEBUG_PANELS && isLocked && (
        <Lock src="/assets/images/panels/lock.png" />
      )}
      {!IS_DEBUG_PANELS && isBehindCover && (
        <CoverPanel onCoverClick={onCoverClick} />
      )}
      {children}
    </Wrapper>
  );
}
