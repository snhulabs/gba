import styled, { css } from 'styled-components';

interface Props {
  span: number;
  className?: string;
  isEnabled?: boolean;
}

export default styled.div<Props>`
  position: relative;
  grid-column: span ${({ span }) => span};
  overflow: hidden;
  width: 100%;
  height: 100%;
`;
