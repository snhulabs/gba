import styled from 'styled-components';

export default styled.div`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);

  cursor: pointer;
`;
