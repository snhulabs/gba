import React from 'react';

import Wrapper from './styled/Wrapper';
import Content from './styled/Content';

export default function FadingScript({ children, time }) {
  return (
    <Wrapper>
      <Content time={time}>{children}</Content>
    </Wrapper>
  );
}
