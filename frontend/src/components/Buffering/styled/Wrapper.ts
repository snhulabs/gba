import styled, { keyframes, css } from 'styled-components';

const loadAnimation = keyframes`
  0%,
  80%,
  100% {
    opacity: 0;
  }
  40% {
    opacity: 1;
  }
`;

interface Props {
  isLoading: boolean;
}

export default styled.div<Props>`
  position: relative;

  border-radius: 50%;
  width: 2.5em;
  height: 2.5em;
  animation-fill-mode: both;
  animation: ${loadAnimation} 1.8s infinite ease-in-out;

  background-color: ${({ theme }) => theme.colors.action};
  font-size: 10px;

  text-indent: -9999em;
  transform: translateZ(0);
  animation-delay: -0.2s;

  &:before,
  &:after {
    content: '';
    position: absolute;
    top: 0;

    border-radius: 50%;
    width: 2.5em;
    height: 2.5em;
    background-color: ${({ theme }) => theme.colors.action};
    animation-fill-mode: both;
    animation: ${loadAnimation} 1.8s infinite ease-in-out;
  }

  &:before {
    left: -3.5em;
    animation-delay: -0.32s;
  }

  &:after {
    left: 3.5em;
  }

  ${({ isLoading }) =>
    !isLoading &&
    css`
      visibility: hidden;
      &:after,
      &:before {
        visibility: hidden;
      }
    `}
`;
