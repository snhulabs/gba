import React from 'react';

import Wrapper from './styled/Wrapper';
import LoaderWrapper from './styled/LoaderWrapper';

interface BufferingProps {
  isLoading: boolean;
  className?: string;
}

const Buffering: React.FunctionComponent<BufferingProps> = ({
  className,
  isLoading,
}) => (
  <LoaderWrapper className={className}>
    <Wrapper isLoading={isLoading} />
  </LoaderWrapper>
);

export default Buffering;
