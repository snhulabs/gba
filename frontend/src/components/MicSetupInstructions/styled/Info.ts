import styled from 'styled-components';

export default styled.div`
  margin: 4vh 0;
  font-size: 1vw;
`;
