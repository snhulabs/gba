import React from 'react';

import Description from 'components/Description';

import InstructionsWrapper from './styled/InstructionsWrapper';
import Info from './styled/Info';
import InfoGraphic from './styled/InfoGraphic';

export default function MicSetupInstructions({ onImageClick }) {
  return (
    <InstructionsWrapper>
      <Description>
        To proceed, allow your browser to
        <br /> access your computer&apos;s microphone.
      </Description>
      <Info>When the message below appears, click “Allow”.</Info>
      <InfoGraphic
        src="/assets/images/micSetupInfo.png"
        onClick={onImageClick}
      />
    </InstructionsWrapper>
  );
}
