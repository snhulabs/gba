import React from 'react';
import Wrapper from './styled/Wrapper';

interface Props {
  instruction: string;
  className?: string;
}

export default function InstructionPanel({ instruction, className }: Props) {
  return <Wrapper className={className}>{instruction}</Wrapper>;
}
