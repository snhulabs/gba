import styled, { css } from 'styled-components';

interface Props {
  inline?: any;
}

export default styled.div<Props>`
  display: flex;
  flex-direction: column;
  align-items: center;

  font-family: Palatino;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  text-align: center;

  ${({ inline }) =>
    inline &&
    css`
      flex-direction: row;
    `};
`;
