import styled, { css } from 'styled-components';

interface Props {
  inline?: any;
}

export default styled.p<Props>`
  ${({ inline }) =>
    inline &&
    css`
      order: 1;
      text-align: start;
    `};
`;
