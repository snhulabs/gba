import styled from 'styled-components';
import { motion } from 'framer-motion';

export default styled(motion.div)`
  // min-height: 35rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
