import styled from 'styled-components';

interface Props {
  size?: any;
}

export default styled.img<Props>`
  width: ${({ size }) => size}%;
  margin: 1rem;
`;
