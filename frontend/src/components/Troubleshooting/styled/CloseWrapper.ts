import styled from 'styled-components';

export default styled.div`
  position: absolute;
  right: 3rem;
  top: 3rem;
  cursor: pointer;
`;
