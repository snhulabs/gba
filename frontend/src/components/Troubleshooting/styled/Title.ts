import styled from 'styled-components';

export default styled.h3`
  font-family: Palatino;
  font-style: normal;
  font-weight: normal;
  font-size: 36px;
  line-height: 36px;
  letter-spacing: 0.025em;
  text-align: center;
`;
