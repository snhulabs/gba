import React, { useEffect } from 'react';
import type { AppProps } from 'next/app';
import { useRouter } from 'next/router';
import { ThemeProvider } from 'styled-components';
import { AnimatePresence } from 'framer-motion';

import '../fonts.css';

import Version from 'u9/components/Version';
import Head from 'u9/components/Head';

import GlobalStyles from 'utils/global-styles';
import theme from 'utils/theme';

import { AudioProvider } from 'context/audio.context';
import { AppProvider, AuthProvider } from 'context';
import Preloader from 'components/Preloader';
import Header from 'components/Header';

const App = ({ Component, pageProps, router }: AppProps) => {
  return (
    // @ts-ignore
    <ThemeProvider theme={theme}>
      <Head title="GBA" description="GBA" />
      <Preloader />
      <AudioProvider>
        <AuthProvider>
          <AppProvider>
            <GlobalStyles />
            <AnimatePresence exitBeforeEnter={true} initial={false}>
              <Component key={router.route} router={router} {...pageProps} />
            </AnimatePresence>
            {process.env.ENV !== 'local' &&
              process.env.ENV !== 'production' && <Version />}
          </AppProvider>
        </AuthProvider>
      </AudioProvider>
    </ThemeProvider>
  );
};

export default App;
