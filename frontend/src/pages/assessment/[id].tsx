export { default, getStaticProps } from 'containers/AssessmentPage';
import { cmsApiClient } from 'utils/api/cms';
import { gql } from 'apollo-boost';

export async function getStaticPaths() {
  return getPaths();
}

const getPaths = async () => {
  const result = await cmsApiClient.query({
    query: gql`
      {
        assesments {
          id
        }
      }
    `,
  });

  const paths = result.data.assesments.map(assessment => {
    return {
      params: {
        id: assessment.id,
      },
    };
  });

  const links = {
    paths: paths,
    fallback: false,
  };

  return links;
};
