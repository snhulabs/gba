import { EVENT_TYPES } from './config';
import { STEPS, STEPS_CONFIG } from 'hooks/useFlowManager';

// TODO: send path in event's data...
export const getNextStepBasedOnEvent = event => {
  const step = getEventsPath(event);

  const stepIdx = STEPS_CONFIG[step].idx;

  return Object.keys(STEPS_CONFIG).find(
    stepConfig => STEPS_CONFIG[stepConfig].idx === stepIdx
  );
};

const getEventsPath = event => {
  switch (event.type) {
    case EVENT_TYPES.login:
      return STEPS.LOGIN;
    case EVENT_TYPES.setup:
      return STEPS.SETUP_MIC;
    case EVENT_TYPES.setupFail:
      return STEPS.SETUP_MIC;
    case EVENT_TYPES.setupSuccess:
      return STEPS.SETUP_MIC;
    case EVENT_TYPES.tutorialFinished:
      return STEPS.TUTORIAL;
    case EVENT_TYPES.recordingRetry:
      return STEPS.ASSESSMENT;
    case EVENT_TYPES.recordingSubmitted:
      return STEPS.ASSESSMENT;
    case EVENT_TYPES.assessmentFinished:
      return STEPS.HUB;
    case EVENT_TYPES.hub:
      return STEPS.HUB;
    case EVENT_TYPES.ending:
      return STEPS.ENDING_NARRATIVE;
    case EVENT_TYPES.summary:
      return STEPS.SUMMARY;
    case EVENT_TYPES.micTest:
      return STEPS.MIC_TEST;
    default:
      return STEPS.LOGIN;
  }
};
