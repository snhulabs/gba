export const ISR_TIMEOUT = process.env.ENV === 'production' ? 300 : 10;

export enum TASKS {
  MicTest = 'MicTest',
  Tutorial = 'Tutorial',
  Module = 'Module',
}

export enum PANEL_TYPE {
  interactive = 'interactive',
  resulting = 'resulting',
  regular = 'regular',
}
export enum EVENT_TYPES {
  login = 'login',
  setup = 'setup',
  setupFail = 'setupFail',
  setupSuccess = 'setupSuccess',
  tutorialFinished = 'tutorialFinished',
  recordingRetry = 'recordingRetry',
  recordingSubmitted = 'recordingSubmitted',
  assessmentFinished = 'assessmentFinished',
  hub = 'hub',
  ending = 'ending',
  summary = 'summary',
  micTest = 'micTest',
}

export const ATTEMPTS_LIMIT = 3;

export const SPEECH_TYPES = ['Competence', 'Likeability', 'Blended'];
export const MIC_ONE_LENGTH = 10000;
export const RECORDING_TIMEOUT = 60000;
export const TUTORIAL_PAUSE_AFTER_RECORDING = 5000;

export const IS_DEBUG_ROUTING = false; // if true, there won't be any redirecting and module checking
export const IS_DEBUG_PANELS = false; // if true resulting panel is shown
