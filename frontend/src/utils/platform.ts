import UAParser from 'ua-parser-js';
import Bowser from 'bowser';

export const isBrowser = () => typeof window !== 'undefined';
export const UA = new UAParser();
export const { name } = UA.getBrowser();
export const { device, os } = UA.getResult();

export const isTouchDevice = () =>
  (isBrowser() && 'ontouchstart' in window) ||
  'ontouchstart' in document.documentElement ||
  navigator.maxTouchPoints > 0 ||
  // @ts-ignore
  navigator.MaxTouchPoints > 0 ||
  navigator.msMaxTouchPoints > 0;

export const isIOS = () => os.name === 'iOS';
export const isAndroid = () => os.name === 'Android';
export const isMobile = () => device.type === 'mobile';
export const isTablet = () => device.type === 'tablet' || isRecentIPadSafari();
export const isDesktop = () => !isMobile() && !isTablet();
export const isIE = () => isBrowser() && name.indexOf('IE') > -1;

// As of Safari iOS 13, iPads return UA strings identical to Mac OS, instead of iOS
export const isRecentIPadSafari = () =>
  isTouchDevice() && isSafari() && !isIOS();
export const isSafari = () =>
  isBrowser() && name.toLowerCase().indexOf('safari') > -1;
export const getUA = () =>
  isBrowser() ? navigator.userAgent || navigator.vendor : '';

// Social Browsers

const isFacebook = () =>
  isBrowser() && (getUA().indexOf('FBAN') > -1 || getUA().indexOf('FBAV') > -1);
const isInstagram = () => isBrowser() && getUA().indexOf('Instagram') > -1;
const isTwitter = () => isBrowser() && getUA().indexOf('Twitter') > -1;
const isLinkedin = () => isBrowser() && getUA().indexOf('LinkedInApp') > -1;

export const isSocialBrowser = () =>
  isFacebook() || isInstagram() || isTwitter() || isLinkedin();

export const getEnvData = () => {
  const bowser = Bowser.getParser(window.navigator.userAgent);

  return {
    device: bowser.getPlatformType(),
    browser: bowser.getBrowserName(),
    version: bowser.getBrowserVersion(),
  };
};
