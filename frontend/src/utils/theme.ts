export default {
  colors: {
    black: '#000000',
    white: '#ffffff',
    background: '#0A3370',
    action: '#FDB913',
    gray: '#bebebe',
    grayDark: '#666666',
    blueOpacity: 'rgba(10, 51, 112, 0.1)',
    lightGray: '#666666',
  },
  fonts: {
    palatino: 'Palatino',
    helvetica: 'Helvetica',
    montserrat: 'Montserrat',
    montserratBold: 'Montserrat-Bold',
  },
};
