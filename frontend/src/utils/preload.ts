export const preloadImages = async (images, cb) => {
  for (let i = 0; i < images.length; i += 1) {
    await preloadImage(images[i]);
  }

  cb();
};

const preloadImage = async image => {
  return new Promise(resolve => {
    const img = new Image();
    img.src = image;
    img.onload = resolve;
    // img.addEventListener('load', resolve);
  });
};
