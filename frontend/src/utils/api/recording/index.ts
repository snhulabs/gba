import LoginForm from 'components/LoginForm';

export const api = process.browser && 'http://localhost:1337';

export const updateRecordingBuffer = (buffer, indexUpload, taskId, token) => {
  //Put request
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ data: buffer, index: indexUpload, token: token }),
  };

  return fetch(`${api}/audioitems/${taskId}`, requestOptions) //need to change that link to something more pretty
    .then(response => response.json());
};

export const startRecording = (token: string) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ token: token }),
  };

  return fetch(`${api}/audioitems`, requestOptions) //need to change that link to something more pretty
    .then(response => response.json());
};

export const sendAudio = async (fileName, blob, recordingId) => {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    const data = new FormData();

    data.append('files', blob, fileName);
    data.append('refId', recordingId);
    data.append('ref', 'recording');
    data.append('field', 'audio');

    req.onload = e => {
      resolve(JSON.parse(req.response));
    };

    req.open('POST', `${api}/upload`, true);
    req.send(data);
  });
};
