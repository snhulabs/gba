import { axios } from 'utils/api/cms';

export const getUser = async token => {
  const response = await axios.get('/users/me', {
    headers: { Authorization: `Bearer ${token}` },
  });

  return response.data;
};

export const getUserEvents = async token => {
  const response = await axios.get('/events', {
    headers: { Authorization: `Bearer ${token}` },
  });

  return response.data;
};

export const getNextModule = async token => {
  const response = await axios.get('/exam/next', {
    headers: { Authorization: `Bearer ${token}` },
  });

  return response.data;
};

export const createUser = async data => {
  const response = await axios.post('/auth/local/register', {
    ...data,
  });

  return response.data;
};

export const loginOrCreate = async userId => {
  const response = await axios.post('/auth/local', { identifier: userId });

  return response.data;
};

export const sendEvent = async (token, userId, type, session, data) => {
  const response = await axios.post(
    '/events',
    { user: userId, type, data, session },
    {
      headers: { Authorization: `Bearer ${token}` },
    }
  );

  return response.data;
};

export const submitRecording = async token => {
  const response = await axios.post('/recordings', {
    headers: { Authorization: `Bearer ${token}` },
  });

  return response.data;
};

export const transcribe = async (token, id) => {
  const response = await axios.get(`/recordings/transcribe/${id}`, {
    headers: { Authorization: `Bearer ${token}` },
  });

  return response.data;
};

export const pushSession = async (token, data) => {
  const response = await axios.post(`/sessions`, data, {
    headers: { Authorization: `Bearer ${token}` },
  });

  return response.data;
};
