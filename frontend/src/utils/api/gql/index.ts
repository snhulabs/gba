import { gql, DocumentNode } from 'apollo-boost';

export const assessmentsQuery = (id): DocumentNode => gql`
  {
    assesments(where: { id: "${id}" }) {
      key
      id
      panels {
        key
        span
        panelImage {
          url
        }
        type
        index
        bubbles {
          text
          x
          y
          width
          type,
          orientation
          pointer {
            orientation,
            thickness,
            direction {
              x,
              y
            }
          }
        }
      }
    }
  }
`;

export const tutorialQuery = (): DocumentNode => gql`
  {
    assesments(where: { type: "tutorial" }) {
      key
      panels {
        key
        panelImage {
          url
        }
        type
        index
        bubbles {
          text
          x
          y
          width
          type
          orientation
          pointer {
            orientation
            thickness
            direction {
              x
              y
            }
          }
        }
      }
    }
  }
`;

export const endingQuery = (): DocumentNode => gql`
  {
    assesments(where: { type: "ending" }) {
      key
      title
      panels {
        key
        panelImage {
          url
        }
        type
        index
        bubbles {
          text
          x
          y
          width
          type
          orientation
          pointer {
            orientation
            thickness
            direction {
              x
              y
            }
          }
        }
      }
    }
  }
`;

export const examQuery = (): DocumentNode => gql`
  {
    exam {
      module {
        id
        title
        text
        assessment {
          id
        }
      }
      randomModules {
        id
        title
        text
        assessment {
          id
        }
      }
      panel2 {
        url
      }
      panel3 {
        url
      }
    }
  }
`;
