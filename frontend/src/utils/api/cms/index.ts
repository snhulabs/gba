import {
  ApolloClient,
  InMemoryCache,
  createHttpLink,
  DefaultOptions,
} from '@apollo/client';
import fetch from 'isomorphic-unfetch';
import Axios from 'axios';

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'cache-and-network',
    errorPolicy: 'all',
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
  mutate: {
    errorPolicy: 'all',
  },
};

const httpLink = createHttpLink({
  uri: 'http://localhost:1337/graphql',
  fetch,
});

const graphqlURL = 'http://localhost:1337/graphql';

const cmsUrl = graphqlURL.replace('/graphql', '');

export const axios = Axios.create({
  baseURL: cmsUrl,
});

export const cmsApiClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
  defaultOptions,
});
