import React from 'react';
import { NextRouter } from 'next/router';
import { RouterContext } from 'next/dist/next-server/lib/router-context';

export function withTestRouter(
  router: Partial<NextRouter> = {},
  tree: React.ReactElement
) {
  const {
    route = '',
    isLocaleDomain = true,
    pathname = '',
    query = {},
    asPath = '',
    push = async () => true,
    replace = async () => true,
    reload = () => null,
    back = () => null,
    prefetch = async () => undefined,
    beforePopState = () => null,
    isFallback = false,
    events = {
      on: () => null,
      off: () => null,
      emit: () => null,
    },
    basePath = '',
    isReady = true,
    isPreview = true,
  } = router;

  return (
    <RouterContext.Provider
      value={{
        route,
        pathname,
        query,
        asPath,
        push,
        replace,
        reload,
        back,
        prefetch,
        beforePopState,
        isFallback,
        events,
        basePath,
        isLocaleDomain,
        isReady,
        isPreview,
      }}
    >
      {tree}
    </RouterContext.Provider>
  );
}
