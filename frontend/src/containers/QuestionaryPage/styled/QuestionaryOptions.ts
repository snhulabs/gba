import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;

  margin: 2vw 0;
`;
