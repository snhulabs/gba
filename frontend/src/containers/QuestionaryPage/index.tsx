import React, { useState, useContext } from 'react';
import { GetStaticProps } from 'next';

import { getEnvData } from 'utils/platform';

import Title from 'components/Title';
import QuestionaryButton from 'components/QuestionaryButton';
import Description from 'components/Description';
import ActionButton from 'components/ActionButton';

import PageContent from './styled/PageContent';
import QuestionaryWrapper from './styled/QuestionaryWrapper';
import QuestionaryOptions from './styled/QuestionaryOptions';
import { AuthContext } from 'context';
import { pushSession } from 'utils/api/rest';
import { useFlowManager } from 'hooks';
import { STEPS } from 'hooks/useFlowManager';
import { motion } from 'framer-motion';

const questionaryTempData = [
  { idx: 0, text: 'Alone indoors' },
  { idx: 1, text: 'Indoors with other people' },
  { idx: 2, text: 'Outside alone' },
  { idx: 3, text: 'Outside with other people' },
];

export const getStaticProps: GetStaticProps = async context => {
  return new Promise(resolve => {
    resolve();
  }).then(() => ({
    props: {
      questionaryData: questionaryTempData,
    },
  }));
};

export default function QuestionaryPage({ questionaryData, ...restProps }) {
  const [selectedQuestionaryOption, setSelectedQuestionaryOption] = useState(
    null
  );
  const { user, JWT, createSession } = useContext(AuthContext);
  const { goToStep } = useFlowManager();

  const selectOption = option => () => {
    setSelectedQuestionaryOption(option);
  };

  const renderOptions = () =>
    questionaryData.map(option => (
      <QuestionaryButton
        selected={option.idx === selectedQuestionaryOption}
        key={option.idx}
        onClick={selectOption(option.idx)}
      >
        {option.text}
      </QuestionaryButton>
    ));

  const onProceed = async () => {
    const { device, browser } = getEnvData();

    const session = await pushSession(JWT, {
      user: user.id,
      environment: questionaryData[selectedQuestionaryOption].text,
      browserUserAgent: device,
      deviceTypeUserAgent: browser,
    });
    createSession(session);

    goToStep(STEPS.SETUP_MIC);
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        <Title>Where Are You Performing This Test?</Title>
        <QuestionaryWrapper>
          <Description>
            For the most accurate results, find a quiet place on your own.
            <br />
            Select the environment below that best describes where you are.
          </Description>
          <QuestionaryOptions>{renderOptions()}</QuestionaryOptions>
        </QuestionaryWrapper>
        <ActionButton
          onClick={onProceed}
          disabled={selectedQuestionaryOption === null}
        >
          Next
        </ActionButton>
      </PageContent>
    </motion.div>
  );
}
