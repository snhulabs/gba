import React, { useEffect, useState } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import { cmsApiClient } from 'utils/api/cms';
import { tutorialQuery } from 'utils/api/gql';
import { EVENT_TYPES, TUTORIAL_PAUSE_AFTER_RECORDING } from 'utils/config';
import useEventManager from 'hooks/useEventManager';
import { preloadImages } from 'utils/preload';

import Comics from 'components/Comics';
import PageContent from 'components/PageContent';
import Buffering from 'components/Buffering';

import CustomInstructions from './styled/CustomInstructions';
import Title from './styled/Title';
import { useFlowManager } from 'hooks';
import { STEPS } from 'hooks/useFlowManager';

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const [tutorialDataResult] = await Promise.all([
    cmsApiClient.query({
      query: tutorialQuery(),
    }),
  ]);

  return {
    props: {
      data: tutorialDataResult.data.assesments[0],
    },
  };
};

export default function TutorialPage({ data }) {
  const { submitEvent } = useEventManager();
  const { goToStep } = useFlowManager();
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    const images = data.panels
      .filter(panel => !!panel.panelImage)
      .map(panel => panel.panelImage.url);
    preloadImages(images, finishLoading);
  }, []);

  const finishLoading = () => {
    setLoaded(true);
  };

  const onSubmit = async data => {
    await submitEvent(EVENT_TYPES.tutorialFinished, data);

    setTimeout(() => goToStep(STEPS.HUB), TUTORIAL_PAUSE_AFTER_RECORDING);
  };

  const onRetry = data => {
    submitEvent(EVENT_TYPES.recordingRetry, data);
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        <Title>Tutorial</Title>
        {!isLoaded ? (
          <Buffering isLoading />
        ) : (
          <Comics onSubmit={onSubmit} onRetry={onRetry} data={data}>
            {/* Very dirty but it's a new requirement that does not follow the structure */}
            <CustomInstructions src="/assets/images/panels/tutorial_interactive_instructions.png" />
          </Comics>
        )}
      </PageContent>
    </motion.div>
  );
}
