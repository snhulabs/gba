import styled from 'styled-components';
import Title from 'components/Title';

export default styled(Title)`
  padding-top: 0;
  margin: 1.5rem 0;
`;
