import styled from 'styled-components';

export default styled.img`
  position: absolute;
  bottom: 0;
  left: 0;

  width: 23%;
`;
