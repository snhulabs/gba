import React, { useState, useEffect, useContext } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import { cmsApiClient } from 'utils/api/cms';
import { assessmentsQuery } from 'utils/api/gql';

import Comics from 'components/Comics';
import Header from 'components/Header';

import { EVENT_TYPES, IS_DEBUG_ROUTING } from 'utils/config';
import useEventManager from 'hooks/useEventManager';
import { ComicDataType } from 'types/assessment';
import { preloadImages } from 'utils/preload';
import Buffering from 'components/Buffering';
import { useFlowManager } from 'hooks';
import { AppContext } from 'context';
import { STEPS } from 'hooks/useFlowManager';

import AssessmentTitle from './styled/AssessmentTitle';
import PageContent from './styled/PageContent';

export const getStaticProps: GetStaticProps = async context => {
  const [assessmentsQueryResult] = await Promise.all([
    cmsApiClient.query({
      query: assessmentsQuery(context.params.id),
    }),
  ]);

  return {
    props: {
      assessment: assessmentsQueryResult.data.assesments[0],
    },
  };
};

interface Props {
  assessment: ComicDataType;
}

export default function AssessmentPage({ assessment, ...restProps }: Props) {
  const { submitEvent } = useEventManager();
  const [isLoaded, setLoaded] = useState(false);
  const { goToStep } = useFlowManager();
  const { moveNext, currentModule } = useContext(AppContext);

  useEffect(() => {
    const images = assessment.panels
      .filter(panel => !!panel.panelImage)
      .map(panel => panel.panelImage.url);
    preloadImages(images, finishLoading);
  }, []);

  const finishLoading = () => {
    setLoaded(true);
  };

  const onSubmit = async data => {
    await submitEvent(EVENT_TYPES.recordingSubmitted, data);
  };

  const onEndCall = async () => {
    await submitEvent(EVENT_TYPES.assessmentFinished, { id: assessment.id });

    const isNext = await moveNext();

    if (!IS_DEBUG_ROUTING) {
      if (isNext) {
        goToStep(STEPS.HUB);
      } else {
        goToStep(STEPS.ENDING_NARRATIVE);
      }
    }
  };

  const onRetry = data => {
    submitEvent(EVENT_TYPES.recordingRetry, data);
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        {currentModule && (
          <AssessmentTitle>{currentModule.title}</AssessmentTitle>
        )}
        {!isLoaded ? (
          <Buffering isLoading />
        ) : (
          <Comics
            onEndCall={onEndCall}
            onSubmit={onSubmit}
            onRetry={onRetry}
            data={assessment}
          />
        )}
      </PageContent>
    </motion.div>
  );
}
