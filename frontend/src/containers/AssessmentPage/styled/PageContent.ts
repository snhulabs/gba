import styled from 'styled-components';
import PageContent from 'components/PageContent';

export default styled(PageContent)`
  justify-content: flex-start;
`;
