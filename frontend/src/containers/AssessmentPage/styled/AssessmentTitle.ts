import styled from 'styled-components';

export default styled.div`
  width: 100%;
  text-align: center;

  font-size: 1.5rem;
  margin: 1.5rem 0;
`;
