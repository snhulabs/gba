import React from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

// import { useLocalizedRouter } from 'hooks';

import Title from 'components/Title';
import MicSetupInstructions from 'components/MicSetupInstructions';
import MicSetupError from 'components/MicSetupError';
import ActionButton from 'components/ActionButton';

import PageContent from './styled/PageContent';
import Footer from './styled/Footer';
import { AudioContext } from 'context/audio.context';
import { EVENT_TYPES } from 'utils/config';
import useEventManager from 'hooks/useEventManager';
import { useFlowManager } from 'hooks';
import { STEPS } from 'hooks/useFlowManager';

export const getStaticProps: GetStaticProps = async context => {
  return new Promise(resolve => {
    resolve();
  }).then(() => ({ props: {} }));
};

interface MicSetupPageProps {}

const MicSetupPage: React.FunctionComponent<MicSetupPageProps> = ({
  ...restProps
}) => {
  const { microphoneEnabled, permissionChecked } = React.useContext(
    AudioContext
  );
  const { submitEvent } = useEventManager();
  const { goToStep } = useFlowManager();
  const onActionButtonClick = React.useCallback(() => {
    if (microphoneEnabled && permissionChecked) {
      submitEvent(EVENT_TYPES.setupSuccess);
      goToStep(STEPS.MIC_TEST);
    } else if (!microphoneEnabled && permissionChecked) {
      submitEvent(EVENT_TYPES.setupFail);
    }
  }, [microphoneEnabled, permissionChecked]);

  React.useEffect(() => {
    if (microphoneEnabled && permissionChecked) {
      submitEvent(EVENT_TYPES.setupSuccess);
      goToStep(STEPS.MIC_TEST);
    }
  }, [microphoneEnabled, permissionChecked]);

  return (
    permissionChecked && (
      <motion.div
        initial={{ opacity: 0, x: 50 }}
        animate={{ opacity: 1, x: 0 }}
        exit={{ opacity: 0, x: -50 }}
        {...restProps}
      >
        <PageContent>
          <Title>Mic Setup</Title>
          {!microphoneEnabled && permissionChecked ? (
            <MicSetupError />
          ) : (
            <MicSetupInstructions onImageClick={onActionButtonClick} />
          )}
          <Footer>
            {microphoneEnabled && permissionChecked && (
              <ActionButton onClick={onActionButtonClick}>Next</ActionButton>
            )}
          </Footer>
        </PageContent>
      </motion.div>
    )
  );
};

export default MicSetupPage;
