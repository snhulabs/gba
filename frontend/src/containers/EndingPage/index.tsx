import React, { useEffect, useState } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import { cmsApiClient } from 'utils/api/cms';
import { endingQuery } from 'utils/api/gql';
import { EVENT_TYPES, TUTORIAL_PAUSE_AFTER_RECORDING } from 'utils/config';
import useEventManager from 'hooks/useEventManager';
import { preloadImages } from 'utils/preload';

import Comics from 'components/Comics';
import PageContent from 'components/PageContent';
import Buffering from 'components/Buffering';

import { useFlowManager } from 'hooks';
import { STEPS } from 'hooks/useFlowManager';
import EndingTitle from './styled/EndingTitle';
import ActionButton from 'components/ActionButton';
import { useRouter } from 'next/router';

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const [tutorialDataResult] = await Promise.all([
    cmsApiClient.query({
      query: endingQuery(),
    }),
  ]);

  return {
    props: {
      data: tutorialDataResult.data.assesments[0],
    },
  };
};

export default function EndingPage({ data }) {
  const { push } = useRouter();
  const { submitEvent } = useEventManager();
  const { goToStep } = useFlowManager();
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    const images = data.panels
      .filter(panel => !!panel.panelImage)
      .map(panel => panel.panelImage.url);
    preloadImages(images, finishLoading);
  }, []);

  const finishLoading = () => {
    setLoaded(true);
  };

  const onSubmit = async data => {
    // await submitEvent(EVENT_TYPES.tutorialFinished, data);
    // setTimeout(() => goToStep(STEPS.HUB), TUTORIAL_PAUSE_AFTER_RECORDING);
  };

  const onRetry = data => {
    // submitEvent(EVENT_TYPES.recordingRetry, data);
  };

  const handleSubmitButton = async () => {
    push('/end');
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        {!isLoaded ? (
          <Buffering isLoading />
        ) : (
          <>
          <EndingTitle>{data.title}</EndingTitle>
          <Comics onSubmit={onSubmit} onRetry={onRetry} data={data} />
          <ActionButton onClick={handleSubmitButton} isLoading={false}>
            End Game
          </ActionButton>
          </>
        )}
      </PageContent>
    </motion.div>
  );
}
