import React, { useEffect } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import PageContent from 'components/PageContent';

import Lines from './styled/Lines';
import Text from './styled/Text';
import Title from './styled/Title';

export const getStaticProps: GetStaticProps = async context => {
  return new Promise(resolve => {
    resolve();
  }).then(() => ({ props: {} }));
};

interface EndPageProps {}

const EndPage: React.FunctionComponent<EndPageProps> = ({ ...restProps }) => {

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      <PageContent>
        <div />
        <div>
          <Lines />
          <Title>Great job!</Title>
          <Text>
            <br />
            You may now close the browser window and return to any previously provided instructions (if applicable).
            <br />
            <br />
            If you have any feedback or questions about the experience, please contact us at learningsolutions@snhu.edu.
          </Text>
        </div>

        <div />
      </PageContent>
    </motion.div>
  );
};

export default EndPage;
