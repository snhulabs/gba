import styled from 'styled-components';
import Lines from 'components/svg/Lines/Lines';

export default styled(Lines)`
  position: absolute;
  top: 55%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 0;
  width: 30vw;
`;
