import styled from 'styled-components';

export default styled.div`
  width: 80%;
  margin: 0 auto;
  font-size: 24px;
  line-height: 36px;
  text-align: center;
  position: relative;
  z-index: 1;
`;
