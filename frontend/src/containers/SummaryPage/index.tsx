import React from 'react';
import { GetStaticProps } from 'next';

import { ISR_TIMEOUT } from 'utils/config';

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  return {
    props: {},
    revalidate: ISR_TIMEOUT,
  };
};

export default function SummaryPage() {
  return <div>SummaryPage</div>;
}
