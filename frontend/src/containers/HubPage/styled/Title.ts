import styled from 'styled-components';
import Title from 'components/Title';

export default styled(Title)`
  text-align: center;
  padding: 1.5rem;
  width: 100%;
`;
