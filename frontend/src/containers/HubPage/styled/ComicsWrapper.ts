import styled from 'styled-components';

export default styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 25vw 25vw;
  grid-gap: 1vw;

  width: 51vw;

  > div {
    position: relative;

    &:nth-child(3) {
      grid-column: 1 / -1;
    }
  }
`;
