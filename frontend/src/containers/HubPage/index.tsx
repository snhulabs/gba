import React, { useState, useEffect, useContext } from 'react';
import { GetStaticProps } from 'next';
import { useRouter } from 'next/router';

import { cmsApiClient } from 'utils/api/cms';
import { examQuery } from 'utils/api/gql';
import { Exam, Module, BubbleType } from 'types/assessment';
import { PANEL_TYPE, EVENT_TYPES } from 'utils/config';

import Panel from 'components/Panel';

import ComicsWrapper from './styled/ComicsWrapper';
import IncomingCallButton from './styled/IncomingCallButton';
import PageContent from './styled/PageContent';
import Title from './styled/Title';
import useEventManager from 'hooks/useEventManager';
import { motion } from 'framer-motion';
import { AppContext, AuthContext } from 'context';
import { preloadImages } from 'utils/preload';
import Buffering from 'components/Buffering';
import ProgressPanel from 'components/ProgressPanel';

export const getStaticProps: GetStaticProps = async context => {
  const [examQueryResult] = await Promise.all([
    cmsApiClient.query({
      query: examQuery(),
    }),
  ]);

  return {
    props: {
      data: examQueryResult.data.exam,
    },
  };
};

interface Props {
  data: Exam;
}

export default function HubPage({ data }: Props) {
  const { push } = useRouter();
  const { submitEvent } = useEventManager();
  const { currentModule } = useContext(AppContext);
  const { user } = useContext(AuthContext);
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    // const activeModule: Module = data.module[moduleIdx];
    if (currentModule) {
      submitEvent(EVENT_TYPES.hub, {
        moduleId: currentModule.id,
      });
    }

    preloadImages([data.panel2.url, data.panel3.url], finishLoading);
  }, [currentModule]);

  const finishLoading = () => {
    setLoaded(true);
  };

  const goToNextAssessment = () => {
    // const module = user.modulesOrder.find(module => module.id === moduleIdx);
    push(`/assessment/${currentModule.assessment.id}`);
  };

  const renderComics = () => {
    //TODO: when doing result panel you need to check both module pools
    //TODO: also store current module and next module - there is a diff idiot
    // const activeModule: Module = data.module[moduleIdx];

    const bubble: BubbleType = {
      text: currentModule ? currentModule.text : '',
      x: 0.1,
      y: 0,
      width: 0.4,
      type: 'fixed',
      orientation: 'top',
      pointer: {
        orientation: 'bottom',
        thickness: -1,
        direction: {
          x: 0.4,
          y: 0.4,
        },
      },
    };

    return (
      <ComicsWrapper>
        {!isLoaded ? (
          <Buffering isLoading />
        ) : (
          <>
            <ProgressPanel />
            <Panel
              panel={{
                key: `hub_2`,
                panelImage: data.panel2,
                type: PANEL_TYPE.regular,
                index: 1,
                bubbles: [],
                span: 1,
              }}
            />
            <Panel
              panel={{
                key: `hub_3`,
                panelImage: data.panel3,
                type: PANEL_TYPE.regular,
                index: 2,
                bubbles: [bubble],
                span: 2,
              }}
            >
              <IncomingCallButton onClick={goToNextAssessment} />
            </Panel>
          </>
        )}
      </ComicsWrapper>
    );
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
    >
      <PageContent>
        <Title>Start the next call when  you are ready.</Title>
        {renderComics()}
      </PageContent>
    </motion.div>
  );
}
