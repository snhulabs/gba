import styled from 'styled-components';
import Title from 'components/Title';

export default styled(Title)`
  margin-top: 10rem;
`;
