import React from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import LoginForm from 'components/LoginForm';

import { useFlowManager } from 'hooks';

import PageContent from './styled/PageContent';
import Title from './styled/Title';
import { STEPS } from 'hooks/useFlowManager';
import useEventManager from 'hooks/useEventManager';
import { EVENT_TYPES } from 'utils/config';

//TODO: get from cms
const questionaryTempData = [
  { idx: 0, text: 'Alone indoors' },
  { idx: 1, text: 'Indoors with other people' },
  { idx: 2, text: 'Outside alone' },
  { idx: 3, text: 'Outside with other people' },
];

export const getStaticProps: GetStaticProps = async context => {
  return new Promise(resolve => {
    resolve();
  }).then(() => ({
    props: {},
  }));
};

interface LoginPageProps {}

const LoginPage: React.FunctionComponent<LoginPageProps> = ({
  ...restProps
}) => {
  const { goToStep } = useFlowManager();
  const { loginEvent } = useEventManager();

  const onLoginCallback = ({ jwt, user }) => {
    loginEvent(jwt, user.id);
    goToStep(STEPS.QUESTIONARY);
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      <PageContent>
        <Title>Enter Your User ID</Title>
        <LoginForm onLoginCallback={onLoginCallback} />
      </PageContent>
    </motion.div>
  );
};

export default LoginPage;
