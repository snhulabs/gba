import React, { useState, useRef, useEffect, useContext } from 'react';
import { GetStaticProps } from 'next';
import { motion } from 'framer-motion';

import IntroMic from 'components/IntroMic';
import ActionButton from 'components/ActionButton';
import Buffering from 'components/Buffering';
import FadingScript from 'components/FadingScript';

import { AudioContext } from 'context/audio.context';
import { EVENT_TYPES, MIC_ONE_LENGTH, TASKS } from 'utils/config';
import useEventManager from 'hooks/useEventManager';
import { useFlowManager, useMicrophoneRecording, useAuth } from 'hooks';
import { STEPS } from 'hooks/useFlowManager';

import PageContent from './styled/PageContent';
import Title from './styled/Title';
import Footer from './styled/Footer';
import Microphone from './styled/Microphone';
import Content from './styled/Content';
import EndMessage from './styled/EndMessage';
import BufferingWrapper from './styled/BufferingWrapper';
import { getUserEvents } from 'utils/api/rest';
import { useRouter } from 'next/router';
import { AuthContext } from 'context';

export const getStaticProps: GetStaticProps = async context => {
  return new Promise(resolve => {
    resolve();
  }).then(() => ({ props: {} }));
};

interface MicTestPageProps {}

const MicTestPage: React.FunctionComponent<MicTestPageProps> = ({
  ...restProps
}) => {
  const {
    startRecording,
    stopRecording,
    upload,
    isProcessing,
    blob,
  } = useMicrophoneRecording();
  const { submitEvent } = useEventManager();
  const { JWT } = useContext(AuthContext);
  const { push } = useRouter();
  const [isActive, setIsActive] = useState(false);
  const [clickedStart, setClickedStart] = useState(false);
  const [hasFinishedUploading, setHasFinishedUploading] = useState(false);
  const [isCountDown, setIsCountDown] = useState(true);
  const startTimeoutId = useRef<NodeJS.Timeout>(null);

  useEffect(() => {
    return () => {
      startTimeoutId.current && clearTimeout(startTimeoutId.current);
    };
  }, []);

  //TODO: refactor
  useEffect(() => {
    if (blob) {
      (async () => {
        await upload(TASKS.MicTest, 1);
        setHasFinishedUploading(true);

        setTimeout(async () => {
          let userEvents = await getUserEvents(JWT);
          userEvents = userEvents.filter(
            event =>
              event.type == EVENT_TYPES.hub ||
              event.type == EVENT_TYPES.ending ||
              event.type == EVENT_TYPES.summary
          );
          if (userEvents[userEvents.length - 1]) {
            //TODO: use flow manager
            push(userEvents[userEvents.length - 1].data.path);
          } else {
            //TODO: use flow manager
            push('/tutorial');
          }
        }, 1500);
      })();
    }
  }, [blob]);

  const onStart = () => {
    setIsActive(true);
    startTimeoutId.current = setTimeout(onFinished, MIC_ONE_LENGTH);
    startRecording();
    setClickedStart(true);
  };

  const onFinished = async () => {
    await stopRecording();
    setIsActive(false);
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: 50 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0, x: -50 }}
      {...restProps}
    >
      <PageContent>
        <Title>Mic Test</Title>
        <Content>
          <Microphone
            isActive={isActive}
            isChecked={hasFinishedUploading}
            isCountdown={isCountDown}
            time={MIC_ONE_LENGTH}
          />
          {!isActive && !isProcessing && !hasFinishedUploading && !clickedStart && (
            <>
              <IntroMic />
              <ActionButton onClick={onStart}>Start</ActionButton>
            </>
          )}

          {!hasFinishedUploading && isActive && (
            <FadingScript time={MIC_ONE_LENGTH}>
              <div>Hello!</div>
              <div>It&apos;s good to meet you!</div>
            </FadingScript>
          )}
          <BufferingWrapper>
            <Buffering isLoading={isProcessing} />
          </BufferingWrapper>
          {hasFinishedUploading && <EndMessage>Thanks, your recording has been uploaded</EndMessage>}
        </Content>
      </PageContent>
    </motion.div>
  );
};

export default MicTestPage;
