import { useState } from 'react';
import { useRouter } from 'next/router';

export enum STEPS {
  LOGIN = 'LOGIN',
  QUESTIONARY = 'QUESTIONARY',
  SETUP_MIC = 'SETUP_MIC',
  MIC_TEST = 'MIC_TEST',
  TUTORIAL = 'TUTORIAL',
  HUB = 'HUB',
  ASSESSMENT = 'ASSESSMENT',
  ENDING_NARRATIVE = 'ENDING_NARRATIVE',
  SUMMARY = 'SUMMARY',
}

export const STEPS_CONFIG = {
  [STEPS.LOGIN]: {
    path: '/login',
    title: 'Login',
    idx: 1,
  },
  [STEPS.QUESTIONARY]: {
    path: '/questionary',
    title: 'Questionary',
    idx: 2,
  },
  [STEPS.SETUP_MIC]: {
    path: '/setup',
    title: 'Setup Mic',
    idx: 3,
  },
  [STEPS.MIC_TEST]: {
    path: '/micTest',
    title: 'Test mic',
    idx: 4,
  },
  [STEPS.TUTORIAL]: {
    path: '/tutorial',
    title: 'Tutorial',
    idx: 5,
  },
  [STEPS.HUB]: {
    path: '/hub',
    title: 'Hub',
    idx: 6,
  },
  [STEPS.ASSESSMENT]: {
    path: '/assessment',
    title: 'Assessment',
    idx: 7,
  },
  [STEPS.ENDING_NARRATIVE]: {
    path: '/ending',
    title: 'Ending',
    idx: 8,
  },
  [STEPS.SUMMARY]: {
    path: '/summary',
    title: 'Summary',
    idx: 9,
  },
};

export default function useFlowManager() {
  const { push } = useRouter();

  const goToStep = (step: STEPS) => {
    push(STEPS_CONFIG[step].path);
  };

  return { goToStep };
}
