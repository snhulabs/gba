import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import { getUser, loginOrCreate, getUserEvents } from 'utils/api/rest';
import { AuthContextProps } from 'context/AuthContext';
import { EVENT_TYPES } from 'utils/config';

const TOKEN_SESSION_STORAGE_KEY = 'GBA-user-token';
const SESSION_SESSION_STORAGE_KEY = 'GBA-session-id';

export default function useAuth(): AuthContextProps {
  const [JWT, setJWT] = useState(null);
  const [user, setUser] = useState(null);
  const [session, setSession] = useState(null);
  const [useEvents, setUserEvents] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(null);
  const { pathname, push } = useRouter();

  useEffect(() => {
    const cachedToken = sessionStorage.getItem(TOKEN_SESSION_STORAGE_KEY);
    const cachedSession = sessionStorage.getItem(SESSION_SESSION_STORAGE_KEY);

    if (cachedToken && cachedSession) {
      (async () => {
        const user = await getUser(cachedToken);
        if (cachedSession) setSession({ id: cachedSession });

        let userEvents = await getUserEvents(cachedToken);
        setUserEvents(userEvents);

        userEvents = userEvents.filter(
          event =>
            event.type == EVENT_TYPES.hub ||
            event.type == EVENT_TYPES.ending ||
            event.type == EVENT_TYPES.summary
        );

        userEvents.length !== 0 &&
          push(userEvents[userEvents.length - 1].data.path);

        authenticate(cachedToken, user);
      })();
    } else if (!pathname.includes('/login')) {
      push('/login');
    }
  }, []);

  const logout = () => {
    sessionStorage.removeItem(TOKEN_SESSION_STORAGE_KEY);
    sessionStorage.removeItem(SESSION_SESSION_STORAGE_KEY);
    setUser(null);
    setJWT(null);
    setSession(null);
    setIsAuthenticated(false);

    push('/login');
  };

  const notAuthenticated = () => {
    setIsAuthenticated(false);
  };

  const authenticate = (token, userData) => {
    sessionStorage[TOKEN_SESSION_STORAGE_KEY] = token;
    setJWT(token);
    setUser(userData);
    setIsAuthenticated(true);
  };

  const login = async userId => {
    //TODO: implement logic
    try {
      const data = await loginOrCreate(userId);
      setJWT(data.jwt);
      setUser(data.user);
      setIsAuthenticated(true);
      sessionStorage.setItem(TOKEN_SESSION_STORAGE_KEY, data.jwt);

      return data;
    } catch (e) {
      console.log(e);
    }
  };

  const createSession = async session => {
    setSession(session);
    sessionStorage.setItem(SESSION_SESSION_STORAGE_KEY, session.id);
  };

  return {
    JWT,
    user,
    isAuthenticated,
    authenticate,
    login,
    session,
    createSession,
    logout,
  };
}
