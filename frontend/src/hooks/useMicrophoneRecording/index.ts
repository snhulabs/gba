import * as React from 'react';

import Recorder from 'utils/recorder';
import { sendAudio } from 'utils/api/recording';
import { submitRecording, transcribe } from 'utils/api/rest';
import { AuthContext } from 'context';

export type RecordingAPI = {
  clearRecording(): void;
  startRecording(): void;
  stopRecording(): void;
  upload(task: any, attempt: any): Promise<void>;
  audioSrc?: string;
  isProcessing: boolean;
  isRecording: boolean;
  uploadFinished?: boolean;
  blob: Blob;
};

let audioCtx = null;
let recorderLib = null;
let audioInput = null;
let audioStream = null;

const useMicrophoneRecording = (): RecordingAPI => {
  const [uploadFinished, setUploadFinished] = React.useState<boolean>(false);
  const [audioSrc, setAudioSrc] = React.useState<string>('');
  const [isRecording, setIsRecording] = React.useState<boolean>(false);
  const [isProcessing, setIsProcessing] = React.useState<boolean>(false);
  const [blob, setBlob] = React.useState<Blob>(null);
  const { JWT, user } = React.useContext(AuthContext);

  const mediaConfig = {
    audio: true,
    video: false,
  };

  const upload = async (task: any, attempt: any) => {
    setIsProcessing(true);
    let recordingRecord = await submitRecording(JWT);
    const recordingName = `user_${user.id}_task_${task}_attempt_${attempt}.wav`;
    const uploadedAudio = await sendAudio(
      recordingName,
      blob,
      recordingRecord.id
    );
    recordingRecord = transcribe(JWT, recordingRecord.id);
    setIsProcessing(false);
    setUploadFinished(true);

    setBlob(null);

    return recordingRecord;
  };

  const setAudio = async blob => {
    setAudioSrc(URL.createObjectURL(blob));
    setBlob(blob);
  };

  const startRecording = () => {
    const AudioContext =
      window.AudioContext || (window as any).webkitAudioContext;
    audioCtx = new AudioContext();

    navigator.mediaDevices.getUserMedia(mediaConfig).then(stream => {
      audioStream = stream;
      audioInput = audioCtx.createMediaStreamSource(stream);
      recorderLib = new Recorder(audioInput, {
        numChannels: 1,
      });
      recorderLib.record();
      setIsRecording(true);
    });
  };

  const stopRecording = async () => {
    return new Promise(resolve => {
      recorderLib.stop();
      audioStream.getAudioTracks()[0].stop();
      recorderLib.exportWAV(blob => {
        setAudio(blob);

        resolve();
      });

      setIsRecording(false);
    });
  };

  const clearRecording = () => {
    if (recorderLib) {
      recorderLib.stop();
    }

    if (audioCtx) {
      audioCtx.close();
    }

    audioCtx = null;
    recorderLib = null;
    audioInput = null;
    audioStream = null;
    setBlob(null);

    setUploadFinished(false);
    setAudioSrc('');
    setIsRecording(false);
    setIsProcessing(false);
  };

  React.useEffect(() => {
    setUploadFinished(false);

    return () => {
      if (recorderLib) {
        recorderLib.stop();
      }

      if (audioCtx) {
        audioCtx.close();
      }

      audioCtx = null;
      recorderLib = null;
      audioInput = null;
      audioStream = null;
      setBlob(null);
    };
  }, []);

  return {
    clearRecording,
    uploadFinished,
    audioSrc,
    isProcessing,
    isRecording,
    startRecording,
    stopRecording,
    upload,
    blob,
  };
};

export default useMicrophoneRecording;
