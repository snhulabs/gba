import React, { useRef, useEffect } from 'react';

import { EVENT_TYPES } from 'utils/config';
import { sendEvent } from 'utils/api/rest';
import { useRouter } from 'next/router';

import { AuthContext } from 'context';

export default function useEventManager() {
  const { user, JWT, isAuthenticated, session } = React.useContext(AuthContext);
  const queuedEvents = useRef([]);
  const { pathname } = useRouter();

  useEffect(() => {
    if (isAuthenticated) {
      for (const evt of queuedEvents.current) {
        submitEvent(evt.event, evt.data);
      }

      queuedEvents.current = [];
    }
  }, [isAuthenticated]);

  const submitEvent = (event: EVENT_TYPES, data = {}) => {
    if (isAuthenticated)
      sendEvent(JWT, user.id, event, session, { ...data, path: pathname });
    else queueEvent(event, data);
  };

  const queueEvent = (event: EVENT_TYPES, data) => {
    queuedEvents.current.push({ event, data });
  };

  const loginEvent = (JWT, userId) => {
    sendEvent(JWT, userId, EVENT_TYPES.login, session, {});
  };

  return { submitEvent, loginEvent };
}
