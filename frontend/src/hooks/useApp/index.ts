import { useState, useContext, useEffect } from 'react';
import { AppContextProps } from 'context/AppContext';

import { AuthContext } from 'context';
import { getNextModule } from 'utils/api/rest';
import useFlowManager, { STEPS } from 'hooks/useFlowManager';

export default function useApp(): AppContextProps {
  const [currentModule, setCurrentModule] = useState(null);
  const [currentModuleIdx, setCurrentModuleIdx] = useState(0);
  const { goToStep } = useFlowManager();
  const { JWT, user } = useContext(AuthContext);

  useEffect(() => {
    if (user) {
      setup();
    }
  }, [user]);

  const setup = async () => {
    const currentModule = await getNextModule(JWT);
    console.log(currentModule);

    if (currentModule.type !== 'end') {
      setCurrentModule(currentModule);
      setCurrentModuleIdx(
        user.modulesOrder.findIndex(module => module.id === currentModule.id)
      );
    } else {
      goToStep(STEPS.ENDING_NARRATIVE);
    }
  };

  const moveNext = async () => {
    if (user) {
      if (user.modulesOrder.length > currentModuleIdx + 1) {
        setCurrentModule(user.modulesOrder[currentModuleIdx + 1]);
        setCurrentModuleIdx(currentModuleIdx + 1);

        return true;
      }
    }

    return false;
  };

  return {
    currentModule,
    currentModuleIdx,
    moveNext,
  };
}
