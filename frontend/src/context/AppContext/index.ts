import React from 'react';

export interface AppContextProps {
  currentModule: any;
  currentModuleIdx: number;
  // setCurrentModule: (module) => void;

  moveNext: () => Promise<boolean>;
}

export const AppContextDefault = {
  currentModule: null,
  // setCurrentModule: () => {},
  currentModuleIdx: 0,
  moveNext: async () => true,
};

export default React.createContext<AppContextProps>(AppContextDefault);
