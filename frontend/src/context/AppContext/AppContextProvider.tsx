import React from 'react';

import useApp from 'hooks/useApp';

import AppContext from './index';

export default function AppContextProvider({ children }) {
  const auth = useApp();

  return <AppContext.Provider value={auth}>{children}</AppContext.Provider>;
}
