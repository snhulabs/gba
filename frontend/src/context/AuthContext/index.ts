import React from 'react';

export interface AuthContextProps {
  JWT: string;
  user: any;
  isAuthenticated: boolean;
  session: any;
  authenticate: (token, user) => void;
  login: (userId) => any;
  createSession: (session) => void;
  logout: () => void;
}

export const AuthContextDefault = {
  JWT: null,
  user: null,
  isAuthenticated: false,
  session: null,
  authenticate: (token, user) => {},
  login: () => {},
  createSession: session => {},
  logout: () => {},
};

export default React.createContext<AuthContextProps>(AuthContextDefault);
