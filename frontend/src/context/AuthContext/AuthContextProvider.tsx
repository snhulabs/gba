import React from 'react';

import { useAuth } from 'hooks';

import AuthContext from './index';

export default function AuthContextProvider({ children }) {
  const auth = useAuth();

  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}
