import { useEffect, createContext, useState } from 'react';
import { useRouter } from 'next/router';

import Recorder, { SAMPLE_RATE } from 'utils/recorder';

export const AudioContext = createContext({
  audioRecorder: null,
  audioBuffer: null,
  audioChunks: null,
  microphoneEnabled: false,
  permissionChecked: false,
  clearAudio: null,
  requestMicrophonePermissions: null,
  audioStream: null,
  task: null,
  setTask: null,
});

export const AudioProvider = ({ children }) => {
  const [permissionChecked, setPermissionChecked] = useState<boolean>(false);
  const [microphoneEnabled, setMicrophoneEnabled] = useState<boolean>(false);
  const [audioRecorder, setAudioRecorder] = useState<Recorder>(null);
  const [audioBuffer, setAudioBuffer] = useState<ArrayBuffer>(null);
  const [audioChunks, setAudioChunks] = useState<[]>(null);
  const [audioStream, setAudioStream] = useState<MediaStream>(null);
  const [task, setTask] = useState(null);

  const { pathname, push } = useRouter();

  const onRecordUpdate = () => {};

  const onRecordFinish = (outputBuffer, chunks) => {
    setAudioBuffer(outputBuffer);
    setAudioChunks(chunks);
  };

  const clearAudio = () => {
    setAudioBuffer(null);
    setAudioChunks(null);
  };

  const requestMicrophonePermissions = () => {
    navigator.mediaDevices
      .getUserMedia({
        audio: {
          advanced: [{ channelCount: 1, sampleRate: SAMPLE_RATE }],
        },
        video: false,
      })
      .then(stream => {
        //const recorder = new Recorder(stream, onRecordUpdate, onRecordFinish);
        setAudioStream(stream);
        setPermissionChecked(true);
        setMicrophoneEnabled(true);
        // setAudioRecorder(recorder);
      })
      .catch(() => {
        setAudioStream(null);
        setPermissionChecked(true);
        setMicrophoneEnabled(false);
      });
  };

  useEffect(() => {
    if (
      /setup|micone|mictwo|micdone|interview/.test(pathname) &&
      !permissionChecked
    ) {
      //TODO add the extra tasks naming
      requestMicrophonePermissions();
    }
  }, [pathname, permissionChecked, microphoneEnabled]);

  return (
    <AudioContext.Provider
      value={{
        audioBuffer,
        audioChunks,
        audioRecorder,
        microphoneEnabled,
        permissionChecked,
        clearAudio,
        requestMicrophonePermissions,
        audioStream,
        task,
        setTask,
      }}
    >
      {children}
    </AudioContext.Provider>
  );
};
