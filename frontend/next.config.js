const withSourceMaps = require('@zeit/next-source-maps');
const withManifest = require('next-manifest');
// const SentryWebpackPlugin = require('@sentry/webpack-plugin');
const env = require('./env');
const withImages = require('next-images');
let icons = [];

try {
  icons = require('./pwa_icons.json');
} catch (e) {
  console.log('> [PWA] pwa_icons.json not found.');
}

const manifest = {
  output: 'public',
  name: 'UNIT9 Base-Web',
  short_name: 'Base-Web',
  display: 'standalone',
  theme_color: '#ffffff',
  background_color: '#000000',
  scope: '/',
  start_url: '/',
  icons: icons,
};

module.exports = withSourceMaps(
  withImages(
    withManifest(
      {
        env,
        future: {
          webpack5: true,
        },
        inlineImageLimit: false,
        serverRuntimeConfig: {
          ...env,
        },
        i18n: {
          locales: ['en', 'pl'],
          defaultLocale: 'en',
        },
        images: {
          deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
          imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
          domains: ['storage.googleapis.com'],
          path: '/_next/image',
          loader: 'default',
        },
        pwa: {
          disable: true,
          dest: 'public',
          scope: '/',
          buildExcludes: [/chunks\/images\/.*$/], //we are using next-optimized-images
        },
        manifest: manifest,
        future: {
          webpack5: false,
        },
        webpack: (config) => {
          return config;
        },
      }
    )
  )
);
