const express = require('express');
const next = require('next');
const basicAuth = require('express-basic-auth');

const config = require('./utils/config');
const configureSecurityHeaders = require('./middlewares/configureSecurityHeaders');

const port = process.env.PORT || 3000;
const dev = process.env.NODE_ENV === 'local';
const app = next({ dev });
const handle = app.getRequestHandler();

(async () => {
  await app.prepare();
  const server = express();

  if (
    !dev &&
    config.BASIC_AUTH_PASSWORD !== '' &&
    config.BASIC_AUTH_PASSWORD !== undefined &&
    config.BASIC_AUTH_PASSWORD !== 'disable'
  ) {
    const basicAuthConfig = {
      users: {
        [config.BASIC_AUTH_USERNAME]: config.BASIC_AUTH_PASSWORD,
      },
      challenge: true,
    };
    server.use(basicAuth(basicAuthConfig));
  }

  configureSecurityHeaders(server);

  server.all('*', (req, res) => {
    return handle(req, res);
  });

  await server.listen(port);
  console.log(`> Ready on http://localhost:${port}`); // eslint-disable-line no-console
})();
