//**********************************************************************************//
//
// Below you can find the default and recommended configuration of Security Headers.
// All settings are based on articles and suggestions from the following sources,
// among others:
//
// https://developer.mozilla.org/en-US/docs/Web/HTTP
// https://helmetjs.github.io/
// https://csp-evaluator.withgoogle.com/
// https://webbkoll.dataskydd.net/
// https://content-security-policy.com/
//
// IMPORTANT NOTE!
// If you need to change something here, probably you want to choose an easy way,
// which is never a good option when it comes to the security of your app.
// Rethink your implementation, never use unsafe-inline, and ask DevOps,
// System Architect or one from our Tech Leads for review.
//
// Security Scanners:
// Content Security Policy Evaluator: https://csp-evaluator.withgoogle.com/
// The Mozilla Observatory: https://observatory.mozilla.org/
// Webbkoll: https://webbkoll.dataskydd.net/
// SSL Labs Scanner: https://www.ssllabs.com/ssltest/
//
//**********************************************************************************//

const helmet = require('helmet');
const config = require('../utils/config');

module.exports = function configSecurityHeaders(server) {
  // X-XSS-Protection header is legacy and it is disabled by default by helmet
  // You can find more info about other security headers, which are added by default by helmet here:
  // https://helmetjs.github.io/
  server.use(
    helmet({
      // Strict-Transport-Security header
      hsts: {
        maxAge: 31536000,
        includeSubDomains: true,
        preload: true,
      },
      // X-Frame-Options header
      frameguard: false,
      // Referrer-Policy header
      referrerPolicy: {
        policy: 'no-referrer',
      },
      // Content-Security-Policy header
      contentSecurityPolicy: {
        directives: {
          defaultSrc: ["'self'"],
          fontSrc: ["'self'", 'fonts.gstatic.com', 'data:'],
          scriptSrc: [
            "'self'",
            // process.env.ENV === 'local' ? "'unsafe-eval'" : '',
            // (req, res) => `'nonce-${res.locals.nonce}'`,
            "'unsafe-eval'",
            'www.google-analytics.com',
            'https://browser.sentry-cdn.com',
          ],
          scriptSrcAttr: ["'none'"],
          objectSrc: ["'none'"],
          frameSrc: ['http://localhost:5000'],
          connectSrc: [
            "'self'",
            '*.sentry.io',
            'localhost:1337',
          ],
          imgSrc: [
            "'self'",
            'www.google-analytics.com',
            'storage.googleapis.com',
            'data:',
          ],
          mediaSrc: ["'self'", 'storage.googleapis.com', 'blob:'],
          styleSrc: [
            "'self'",
            // (req, res) => `'nonce-${res.locals.nonce}'`,
            "'unsafe-inline'",
            'fonts.googleapis.com',
          ],
          baseUri: ["'self'"],
          formAction: ["'self'"],
          workerSrc: ['self', 'blob:'],
          manifestSrc: ["'self'"],
          reportUri: [],
          blockAllMixedContent: [],
        },
      },
    })
  );
};
