const UserController = require('strapi-plugin-users-permissions/controllers/User');

module.exports = {
    create: async ctx => {
        ctx.request.body.password='';

        return UserController.create(ctx);
    },
};
