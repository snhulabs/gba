const _ = require("lodash");

const AuthController = require("strapi-plugin-users-permissions/controllers/Auth");
const UserController = require("./User");

const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const getRandomSet = (arr, setSize) => {
  const randSet = [];
  const shuffledArr = arr.sort(() => Math.random() - 0.5)
  for (let i = 0; i < setSize; i += 1) {
    randSet.push(shuffledArr[i]);
  }

  return randSet;
};

module.exports = {
  async callback(ctx) {
    ctx.request.body.password = "";
    const provider = ctx.params.provider || "local";
    const params = ctx.request.body;

    // The identifier is required.
    if (!params.identifier) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.email.provide",
          message: "Please provide your username or your e-mail."
        })
      );
    }

    // The password is required.
    if (!params.password) {
      return ctx.badRequest(
        null,
        formatError({
          id: "Auth.form.error.password.provide",
          message: "Please provide your password."
        })
      );
    }

    const query = { provider };

    // Check if the provided identifier is an email or not.
    const isEmail = emailRegExp.test(params.identifier);

    // Set the identifier to the appropriate query field.
    if (isEmail) {
      query.email = params.identifier.toLowerCase();
    } else {
      query.username = params.identifier;
    }

    // Check if the user exists.
    const user = await strapi.query("user", "users-permissions").findOne(query);

    if (!user) {
      ctx.request.body.email = `${params.identifier}@snhu.com`;
      ctx.request.body.username = params.identifier;

      //TODO: refactor and split to functions
      const exam = await strapi.services.exam.find();
      const randomModules = exam.randomModules;
      const orderedModules = exam.module;
      const finalModulesOrder = [
        ...getRandomSet(orderedModules, 9),
        ...getRandomSet(randomModules, 3)
      ];

      // const firstSetOfModules = await.strapi.services
      ctx.request.body.modulesOrder = finalModulesOrder;

      await UserController.create(ctx);
    }

    return AuthController.callback(ctx);
  }
};
