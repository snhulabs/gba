"use strict";

/**
 * gba-reporting.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

const EVENT_TYPES = {
  LOGIN: "login",
  SETUP: "setup",
  SETUP_FAIL: "setupFail",
  MIC_TEST: "micTest",
  SETUP_SUCCESS: "setupSuccess",
  TUTORIAL_FINISHED: "tutorialFinished",
  RECORDING_RETRY: "recordingRetry",
  RECORDING_SUBMITTED: "recordingSubmitted",
  ASSESSMENT_FINISHED: "assessmentFinished",
  HUB: "hub",
  ENDING: "ending",
  SUMMARY: "summary"
};

const TASK_TYPES = [
  EVENT_TYPES.TUTORIAL_FINISHED,
  EVENT_TYPES.RECORDING_SUBMITTED
];

const hasSuccessfulSetupEvent = (user, events) => {
  const setupEvents = events.find(event => {
    return event.user.id === user.id && event.type === EVENT_TYPES.SETUP_SUCCESS;
  });

  return setupEvents ? setupEvents.length !== 0 : false;
};

const getUserRecordings = (event, recordings) => {
  return event.data.recordingId
    ? recordings.find(recording => recording.id === event.data.recordingId)
    : {};
};

const generateTasks = (user, events, recordings) => {
  return events
    .filter(event => {
      return TASK_TYPES.includes(event.type) && event.user.id === user.id;
    })
    .map(event => {
      const recording = getUserRecordings(event, recordings);

      return {
        id: event.id,
        name: event.data.name,
        date: event.created_at,
        attempt: event.data.attempt || 0,
        transcription: recording.transcription || null,
        confidence: recording.confidence || null,
        audioFile: recording.audio ? recording.audio.url : null,
        speechType: event.data.speechType,
      };
    });
};

module.exports = {
  generateReport: async () => {
    const users = await strapi.plugins[
      "users-permissions"
    ].services.user.fetchAll({ _limit: -1 });

    const sessions = await strapi.query("session").find({ _limit: -1 });
    const events = await strapi.query("event").find({ _limit: -1 });
    const recordings = await strapi.query("recording").find({ _limit: -1 });

    const report = sessions.map(session => {
      const user = users.find(usr => usr.id === session.user.id);

      const sessionEvents = events.filter(event => {
        if (event.session) return event.session.id === session.id;
      });

      return {
        userId: user.username,
        id: session.id,
        environment: session.environment,
        browserUserAgent: session.browserUserAgent,
        deviceTypeUserAgent: session.deviceTypeUserAgent,
        mic_permission: hasSuccessfulSetupEvent(user, sessionEvents),
        mic_extra: null,
        date: session.created_at,
        // step: "?",
        tasks: generateTasks(user, sessionEvents, recordings)
      };
    });

    return report;
  }
};
