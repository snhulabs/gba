"use strict";

/**
 * gba-reporting.js controller
 *
 * @description: A set of functions called "actions" of the `gba-reporting` plugin.
 */

module.exports = {
  /**
   * Default action.
   *
   * @return {Object}
   */

  index: async ctx => {
    const report = await strapi.plugins["gba-reporting"].services[
      "gba-reporting"
    ].generateReport();

    ctx.send({
      message: "ok",
      report
    });
  }
};
