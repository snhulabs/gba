#! /bin/bash

echo ""
echo "Building CMS..."
echo "${STRAPI_UPLOADER_SERVICE_ACCOUNT_KEY}" >.uploader-service-account-key.json

export NODE_ENV=production

npm run feed-env-vars

image="$GCP_GCR_LOCATION.gcr.io/$GCLOUD_PROJECT_ID/$ENV:cms"

if docker pull "$image" > /dev/null; then
  docker build . --tag "$image" --cache-from "$image"
else
  docker build . --tag "$image"
fi

docker push "$image"

gcloud run deploy "$GCLOUD_PROJECT_ID-cms-$ENV" \
  --image "$image" \
  --platform managed \
  --region "$GCP_REGION"

# Synchronize the database
if [ $SYNC_DB_ON_PRODUCTION == "true" ] || [ $ENV != "production" ]
then
  npm i
  npm run sync-db
else
  echo "skipping db sync on production"
fi
