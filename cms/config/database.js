module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        database: 'cms',
        host: 'localhost',
        port: 3308,
        username: 'root',
        password: 'password',
      },
      options: {
        debug: true,
        pool: {
          min: 0,
          max: 15,
          createTimeoutMillis: 3000,
          acquireTimeoutMillis: 30000,
          idleTimeoutMillis: 30000,
          reapIntervalMillis: 1000,
          createRetryIntervalMillis: 100,
        },
      },
    },
  },
});
