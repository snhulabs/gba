module.exports = ({ env }) => ({
  admin: {
    host: "0.0.0.0",
    watchIgnoreFiles: [
      "/usr/src/cms/init.sql"
    ]
  }
});
