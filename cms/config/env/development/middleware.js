module.exports = {
  settings: {
    logger: {
      level: 'debug',
      exposeInContext: true,
      requests: true,
    },
    gzip: {
      enabled: false,
    },
    p3p: {
      enabled: false,
      value: '',
    },
  },
};
