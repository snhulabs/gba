module.exports = {
  settings: {
    logger: {
      level: 'info',
      exposeInContext: true,
      requests: false,
    },
    gzip: {
      enabled: true,
    },
    p3p: {
      enabled: true,
      value: '',
    },
  },
};
