"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  nextExam: async ctx => {
    let events = await strapi.services.event.getEvents(ctx);
    events = events.filter(event => event.type === "assessmentFinished");

    const lastAssessment = events[events.length - 1];
    const userModulesOrder = ctx.state.user.modulesOrder;

    if (!lastAssessment) {
      ctx.send(userModulesOrder[0]);

      return;
    }


    for (let i = 0; i < userModulesOrder.length; i += 1) {
      // required due to some local testing before release
      if (userModulesOrder[i]) {
        if (userModulesOrder[i].assessment.id == lastAssessment.data.id) {
          if (i + 1 < userModulesOrder.length) {
            ctx.send(userModulesOrder[i + 1]);
          } else {
            ctx.send({ id: -1, type: "end" });
          }
          return;
        }
      }
    }
  }
};
