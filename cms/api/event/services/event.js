'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

module.exports = {
    getEvents: async (ctx) => {
        const user = ctx.state.user;
        let events = await strapi.services.event.find({user: user.id});
        events = events.map(({type, created_at, data}) => ({type, created_at, data}))

        return events;
    }
};
