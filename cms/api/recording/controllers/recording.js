'use strict';
const { sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    transcribe: async (ctx) => {
        const { id } = ctx.params;
        const recording = await strapi.services.recording.findOne({id});

        const transcription = await strapi.services.recording.parseAudioLong(`gs://gba-uploads/recording/${recording.audio.hash}${recording.audio.ext}`)

        const updatedRecording = await strapi.services.recording.update({id}, {
            transcription
        });

        return sanitizeEntity(updatedRecording, { model: strapi.models.recording });
    }
};
