'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/services.html#core-services)
 * to customize this service
 */

const speech = require("@google-cloud/speech").v1p1beta1;
const client = new speech.SpeechClient();

module.exports = {
    parseAudioLong: async (file) => {
        // The audio file's encoding, sample rate in hertz, and BCP-47 language code
        const audio = {
          uri: file,
        };
        const config = {
          encoding: "LINEAR16",
          languageCode: "en-US",
          enableAutomaticPunctuation: true,
        };
        const request = {
          audio: audio,
          config: config,
        };
        // Detects speech in the audio file
        try {
          const [operation] = await client.longRunningRecognize(request);
          const [response] = await operation.promise();
        //   const results = response.results.map((result) => console.log(result));

          const transcription = response.results
            .map((result) => result.alternatives[0].transcript)
            .join("\n");

            return transcription;
        } catch (e) {
          console.log(e);
        }
      }
};
