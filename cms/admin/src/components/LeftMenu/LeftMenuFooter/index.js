/**
 *
 * LeftMenuFooter
 *
 */

import React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import axios from 'axios';
import { useLocation } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import LeftMenuLink from '../LeftMenuLink';
import Wrapper from './Wrapper';
import messages from './messages.json';

defineMessages(messages);

const LeftMenuFooter = ({ version }) => {
  const location = useLocation();
  const commitDbLink = {
    icon: 'database',
    label: 'commit',
    onClick: () => {
      const requestUrl = `${strapi.backendURL}/commit-db`;
      axios.post(requestUrl).then(response => {
        if (response.data.ok) {
          strapi.notification.success('notification.dbcommit.success');
        } else {
          strapi.notification.error('notification.dbcommit.error');
        }
      });
    },
  }

  const generateJSON = {
    icon: 'database',
    label: 'reporting',
    onClick: async () => {
      const requestUrl = `${strapi.backendURL}/gba-reporting`;

      const response = await axios.get(requestUrl, {
        headers: { Authorization: `Bearer ${sessionStorage.getItem('jwtToken').replaceAll('"', '')}` },
      })

      if (response.status === 200) {
          strapi.notification.success("notification.generateReport.success");

          const link = document.createElement("a");
          var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(response.data.report));

          link.setAttribute("href", dataStr);
          link.setAttribute("download", "report.json");
          document.body.appendChild(link); // Required for FF
          link.click();
          document.body.removeChild(link);
        } else {
          strapi.notification.error("notification.generateReport.error");
        }
    }};


  const staticLinks = [

  ];
  staticLinks.unshift(generateJSON);

  if (strapi.remoteURL.includes('localhost')) {
    staticLinks.unshift(commitDbLink);
  }

  return (
    <Wrapper>
      <ul className="list">
        {staticLinks.map(link => (
          <LeftMenuLink
            location={location}
            iconName={link.icon}
            label={messages[link.label].id}
            key={link.label}
            destination={link.destination}
            onClick={link.onClick}
          />
        ))}
      </ul>
      <div className="poweredBy">
        <FormattedMessage
          id={messages.poweredBy.id}
          defaultMessage={messages.poweredBy.defaultMessage}
          key="poweredBy"
        />
        <a key="website" href="https://strapi.io" target="_blank" rel="noopener noreferrer">
          Strapi
        </a>
        &nbsp;
        <a
          href={`https://github.com/strapi/strapi/releases/tag/v${version}`}
          key="github"
          target="_blank"
          rel="noopener noreferrer"
        >
          v{version}
        </a>
      </div>
    </Wrapper>
  );
};

LeftMenuFooter.propTypes = {
  version: PropTypes.string.isRequired,
};

export default LeftMenuFooter;
